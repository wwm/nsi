def appartient(animal, ferme):
    return animal in ferme

def nombre_animaux(animal, ferme):
    if appartient(animal, ferme):
        return ferme[animal]
    return 0

ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 1, "cheval": 4}
assert appartient("oie", ferme_gaston) == False
assert nombre_animaux("lapin", ferme_gaston) == 5
