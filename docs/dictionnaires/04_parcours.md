---
author: Mireille Coilhac
title: Parcours
---


## I. Parcourir un dictionnaire

???+ question "Testez"

    Utilisons la même syntaxe que celle employée avec les listes python : 

    {{IDE('scripts/parcours_dico')}}


???+ question

    Nous disposons du dictionnaire : `mon_dictionnaire`.  
    Le parcours avec la boucle `for element in mon_dictionnaire` se fait :

    === "Cocher la ou les affirmations correctes"
        
        - [ ] sur les clés du dictionnaire.
        - [ ] sur les valeurs du dictionnaire.
        - [ ] sur les paires clé, valeur du dictionnaire.
        - [ ] sur les indices du dictionnaires

    === "Solution"
        
        - :white_check_mark: sur les clés du dictionnaire.
        - :x: ~~sur les valeurs du dictionnaire.~~
        - :x: ~~sur les paires clé, valeur du dictionnaire.~~
        - :x: ~~sur les indices du dictionnaires~~ Il n'y a pas d'"indices" pour des dictionnaires

!!! abstract  "🐘  À retenir"

    Soit le dictionnaire `mon_dictionnaire`  
    Le parcours d'un dictionnaire se fait sur ses clés en écrivant :

    ```python
    for cle in mon_dictionnaire :
        ...
    ```

???+ question "Gaston fait ses courses"

    Gaston va faire ses courses dans une boutique "prix ronds". Mais comme il est passionné de Python, il a créé un dictionnaire pour représenter ses achats appelé `achats` et un autre pour représenter le prix de chaque article appelé `prix`. 
    Exemple pour l'achat d'un kg de farine,d'une plaquette de beurre, et d'un kg de sucre.  
    Il connait les prix suivants :  

    |produit|1kg de farine|1 kg de sucre|1 l de lait|1 plaquette de beurre|
    |--|--|--|--|--|
    |prix|1|3|2|2|

    Voici par exemple les dictionnaires :

    ```python
    achats = {"farine": 2, "beurre": 1, "sucre": 2}
    prix = {"farine": 1, "sucre": 3, "lait": 2, "beurre": 2}  
    ```

    * Le dictionnaire `achats` a pour clés les ingrédients et pour valeur le nombre d'ingrédients correspondants achetés.  
    Ici Gaston achète 2kg de farine, 1 plaquette de beurre, 2 kgs de sucre.    
    * Le dictionnaire `prix` a pour clés les ingrédients et pour valeur le prix de chacun.   
    Ici 1kg de farine coûte 1 €, 1kg de sucre coûte 3 € etc.


    Aidez Gaston à compléter la fonction `facture` qui prend en paramètres les deux dictionnaires, et renvoie le montant qu'il devra payer pour ses achats. Les valeurs du dictionnaire `prix` sont des entiers.

    {{IDE('scripts/achats_1')}}

    ??? success "Solution"

        ```python
        def facture(achats, prix) -> int:
            a_payer = 0
            for article in achats:
                a_payer = a_payer + achats[article] * prix[article]
            return a_payer
        ```

## II. Obtenir toutes les valeurs d'un dictionnaire

???+ question "Les notes d'Alice"

    Alice a créé un dictionnaire avec ses notes de NSI.  
    Par exemple `notes_Alice = {"test_1": 14, "test_2": 16, "test_3": 18}`
    Aidez-là à compléter la fonction qui renvoie **une** liste des notes qu'elle a obtenues


    {{IDE('scripts/notes_1')}}

    ??? success "Solution"

        On peut penser à la solution suivante : 

        ```python
        def notes(les_notes) -> list:
            liste_notes = []
            for devoir in les_notes:
                liste_notes.append(les_notes[devoir])
            return liste_notes
        ```

        ??? note "Peut-être n'avez-vous pas pensé à ceci : "

            On peut utiliser une liste en compréhension

            ??? success "Solution"

                ```python
                def notes(les_notes) -> list:
                    liste_notes = [les_notes[devoir] for devoir in les_notes]
                    return liste_notes
                ```

!!! abstract  "🐘  À retenir"

    Soit le dictionnaire `mon_dictionnaire`  
    on peut obtenir une liste `valeurs` de toutes les valeurs de ce dictionnaire ainsi :

    ```pycon
    valeurs = [mon_dictionnaire[cle] for cle in mon_dictionnaire]
    ```


!!! danger "Attention"

    🌵 🌵 Tester ci-dessous

    {{IDE('scripts/notes_ordre')}}

    👉 Il faut se souvenir qu'un dictionnaire **n'est pas ordonné** ...


## III. Obtenir tous les couples (clé, valeur) d'un dictionnaire

???+ question "Les notes d'Alice"

    Alice a créé un dictionnaire avec ses notes de NSI.   
    Par exemple `notes_Alice = {"test_1": 14, "test_2": 16, "test_3": 18}`  
    Aidez-là à compléter la fonction qui renvoie **une** liste de tuples (devoir, note).  

    {{IDE('scripts/notes_2')}}

    ??? success "Solution"

        On peut penser à la solution suivante : 

        ```python
        def bulletin(les_notes) -> list:
            liste_notes = []
            for devoir in les_notes:
                liste_notes.append((devoir, les_notes[devoir]))
            return liste_notes
        ```

        ??? note "Peut-être n'avez-vous pas pensé à ceci : "

            On peut utiliser une liste en compréhension

            ??? success "Solution"

                ```python
                def bulletin(les_notes) -> list:
                    liste_notes = [(devoir, les_notes[devoir]) for devoir in les_notes]
                    return liste_notes
                ```

!!! abstract  "🐘  À retenir"

    Soit le dictionnaire `mon_dictionnaire`  
    on peut obtenir **une** liste des couples (clé, valeur) de ce dictionnaire ainsi :

    ```python
    valeurs = [(cle, mon_dictionnaire[cle]) for cle in mon_dictionnaire]
    ```

!!! danger "Attention"

    🌵 🌵 Tester ci-dessous

    {{IDE('scripts/notes_2_ordre')}}

    👉 Il faut se souvenir qu'un dictionnaire **n'est pas ordonné** ...

 
## IV. Présentation des trois méthodes `keys`, `values` et `items`

!!! danger "Attention"

    Les objets renvoyés par les méthodes keys, values, items sont d’un type particulier,leur connaissance n’est pas au programme. Par contre les méthodes `keys`, `values` et `items`le sont.  
    👉 Remarquons néanmoins que l'on peut toujours se passer de ces méthodes.

???+ question "Testez"

    {{IDE('scripts/methodes_1')}}

???+ question "Les vues"

    🌵 En fait, les objets renvoyés par les méthodes `keys`, `values` et `items` sont des objets que l'on appelle des "vues".  
    👉 Testez ci-dessous et observez ce qui est renvoyé.


    {{IDE('scripts/vues_1')}}

???+ question "transformer des vues en listes Python"

    👉 Testez ci-dessous et observez ce qui est renvoyé.

    {{IDE('scripts/listes_vues')}}

!!! abstract  "obtenir des listes de clés, valeurs, couples (clé, valeur)"

    On peut créer les listes de clés, de valeurs ou de couples (clé, valeur) :
    
    Soit le dictionnaire `mon_dictionnaire`  
    
    * `list(mon_dictionnaire.keys())` permet d'obtenir une liste des clés de `mon_dictionnaire`
    * `list(mon_dictionnaire.values())` permet d'obtenir une liste des valeurs de `mon_dictionnaire`
    * `list(mon_dictionnaire.items())` permet d'obtenir une liste des tuples (clé, valeur) de `mon_dictionnaire`

## V. La méthode `keys`

???+ question "Testez"

    {{IDE('scripts/ferme_1_keys')}}

Ne connaissez-vous pas une autre façon de procéder ?

Nous aurions pu tout simplement écrire

???+ question "Testez"

    {{IDE('scripts/ferme_2_keys')}}
            

???+ question "Parcourir avec la méthode `keys`"

    Testez ci-dessous

    {{IDE('scripts/parcours_keys')}}



!!! abstract  "🐘  À retenir"

    Soit le dictionnaire `mon_dictionnaire`  
    `mon_dictionnaire.keys()` permet de parcourir toutes les clés de `mon_dictionnaire`


???+ question

    `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`

    Que renvoie l'expression `"cheval" in ferme_gaston.keys()` ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `True`
        - [ ] `False`
        - [ ] Une erreur
        - [ ] Rien

    === "Solution"
        
        - :white_check_mark: `True`
        - :x: ~~`False`~~
        - :x: ~~Une erreur~~
        - :x: ~~Rien~~

???+ question

    `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`
    
    Quelle instruction permet de parcourir les clés de `ferme_gaston` ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `for cle in range(ferme_gaston)`
        - [ ] `for cle in ferme_gaston.keys`
        - [ ] `for cle in ferme_gaston.keys()`
        - [ ] `for cle in keys.ferme_gaston()`
        - [ ] `for cle in ferme_gaston`

    === "Solution"
        
        - :x: ~~`for cle in range(ferme_gaston)`~~
        - :x: ~~`for cle in ferme_gaston.keys`~~ Il ne faut pas oublier les parenthèses.
        - :white_check_mark: `for cle in ferme_gaston.keys()`
        - :x: ~~`for cle in keys.ferme_gaston()`~~
        - :white_check_mark: `for cle in ferme_gaston`

        
## VI. La méthode `values`

???+ question "Testez"

    {{IDE('scripts/pokemons_1')}}

???+ question "Parcourir avec la méthode `values`"

    Testez ci-dessous

    {{IDE('scripts/pokemons_2')}}

!!! abstract  "🐘  À retenir"

    Soit le dictionnaire `mon_dictionnaire`  
    `mon_dictionnaire.values()` permet d'accéder à toutes les valeurs de `mon_dictionnaire`

???+ question

    `couleurs = {"white": "blanc", "black": "noir", "red": "rouge"}`

    Que renvoie l'expression `"vert" in couleurs.values()` ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `True`
        - [ ] `False`
        - [ ] Une erreur
        - [ ] Rien

    === "Solution"
        
        - :x: ~~`True`~~ 
        - :white_check_mark: `False`
        - :x: ~~Une erreur~~
        - :x: ~~Rien~~

???+ question

    ```python
    pokemons = {"Salamèche" : "Feu", "Reptincel": "Feu",
                "Carapuce": "Eau", "Chenipan": "Insecte"}
    ```
    
    Quelle instruction permet de parcourir les types des pokémons de ce dictionnaire ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `for type_pokemon in range(pokemons)`
        - [ ] `for type_pokemon in pokemons.values()`
        - [ ] `for type_pokemon in pokemons.values`
        - [ ] `for type_pokemon in values.pokemons()`
        - [ ] `for type_pokemon in pokemons`

    === "Solution"
        
        - :x: ~~`for type_pokemon in range(pokemons)`~~
        - :white_check_mark: `for type_pokemon in pokemons.values()`
        - :x: ~~`for type_pokemon in pokemons.values`~~ Il ne faut pas oublier les parenthèses.
        - :x: ~~`for type_pokemon in values.pokemons()`~~
        - :x: ~~`for type_pokemon in pokemons`~~ Cette instruction parcourt les clés

???+ question "utiliser la méthode `values`"

   
    Bob a constitué un dictionnaire de recettes de la façon suivante : les clés sont les noms des gâteaux, et les valeurs les listes de ingrédients pour chaque gâteau.  
    Aidez-le à écrire la fonction Python qui prend en paramètres un dictionnaire des recettes, un ingrédient, et qui renvoie le nombre de recettes utilisant cet ingrédient.

    {{IDE('scripts/parcours_values')}}

    ??? success "Solution"

        ```python
        def nbre_recettes(recettes, ingredient):
            total = 0
            for ingredients in recettes.values():
                if ingredient in ingredients:
                    total = total + 1
            return total
        ```

        ??? note "et sans la méthode `values` ?"

            Nous aurions pu écrire ceci : 

            ```python
                def nbre_recettes(recettes, ingredient):
                    total = 0
                    for gateau in recettes:
                        if ingredient in recettes[gateau]:
                            total = total + 1
                    return total
            ```

## VII. La méthode `items`

???+ question "Testez"

    {{IDE('scripts/pokemons_3')}}

???+ question "Parcourir avec la méthode `items`"

    Testez ci-dessous

    {{IDE('scripts/pokemons_4')}}

!!! abstract  "🐘  À retenir"

    Soit le dictionnaire `mon_dictionnaire`  
    `mon_dictionnaire.items()` permet d'accéder à tous les tuples (clé, valeur) de `mon_dictionnaire`

???+ question

    `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`

    Que renvoie l'expression `("dragon", 2) in ferme_gaston.items()` ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `True`
        - [ ] `False`
        - [ ] Une erreur
        - [ ] Rien

    === "Solution"
        
        - :x: ~~`True`~~
        - :white_check_mark: `False`
        - :x: ~~Une erreur~~
        - :x: ~~Rien~~

???+ question

    `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`
    
    Quelle instruction permet de parcourir les tuples (animal, effectif) de `ferme_gaston` ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `for paire in range(ferme_gaston)`
        - [ ] `for paire in items.ferme_gaston`
        - [ ] `for paire in ferme_gaston.items`
        - [ ] `for paire in ferme_gaston.items()`
     
    === "Solution"
        
        - :x: ~~`for paire in range(ferme_gaston)`~~
        - :x: ~~`for paire in items.ferme_gaston`~~
        - :x: ~~`for paire in ferme_gaston.items`~~ Il ne faut pas oublier les parenthèses.
        - :white_check_mark: `for paire in ferme_gaston.items()`



???+ question "utiliser la méthode `items`"

    "Testez"
    
    Alice est étourdie, elle ne se souvient plus à quel devoir elle a obtenu la note "18".
    Compléter ci-dessous :

    {{IDE('scripts/parcours_items')}}

    ??? success "Solution"

        ```python
        def intitule(les_notes, note) -> str:
            for (nom, resultat) in les_notes.items():
                if resultat == note:
                    return nom
            return "Cette note n'a pas été obtenue"
        ```

        ??? note "et sans la méthode `items` ?"

            Nous aurions pu écrire ceci : 

            ```python
            def intitule(les_notes, note) -> str:
                for nom  in les_notes :
                    if les_notes[nom] == note:
                        return nom
                return "Cette note n'a pas été obtenue"
            ```


!!! abstract  "Utiliser les méthodes `keys`, `values` et `items`"

    On peut parcourir les vues créées par ces méthodes, de façon analogue à ce que l'on ferait avec d'autres séquences comme des listes :
    
    Soit le dictionnaire `mon_dictionnaire`  
    
    * `mon_dictionnaire.keys()` permet de parcourir toutes les clés de `mon_dictionnaire`
    * `mon_dictionnaire.values()` permet de parcourir toutes les valeurs de `mon_dictionnaire`
    * `mon_dictionnaire.items()` permet de parcourir tous les couples (clé, valeur) de `mon_dictionnaire`


## VIII. QCM final

???+ question

    On considère le dictionnaire`symboles_hexa = {"A": 10, "B": 11, "C": 12, "D": 13, "E":14, "F":15}`

    Que peut afficher `print(list(symboles_hexa.items()))` ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `["A", "B", "C", "D", "E", "F"]`
        - [ ] `[10, 11, 12, 13, 14, 15]`
        - [ ] `[(10,"A"), (11, "B") ,(12, "C"), (13, "D"), (14, "E"), (15, "F")]`
        - [ ] `[("A": 10), ("B": 11), ("C": 12), ("D": 13), ("E": 14), ("F": 15)]`
        - [ ] `[("A", 10), ("B", 11), ("C", 12), ("D", 13), ("E", 14), ("F", 15)]`

    === "Solution"
        
        - :x: ~~`["A", "B", "C", "D", "E", "F"]`~~  
        - :x: ~~`[10, 11, 12, 13, 14, 15]`~~
        - :x: ~~`[(10,"A"), (11, "B") ,(12, "C"), (13, "D"), (14, "E"), (15, "F")]`~~
        - :x: ~~`[("A": 10), ("B": 11), ("C": 12), ("D": 13), ("E": 14), ("F": 15)]`~~
        - :white_check_mark: `[("A", 10), ("B", 11), ("C", 12), ("D", 13), ("E", 14), ("F", 15)]`

???+ question "Liquidation"


    Que peut-il s'afficher si l'on exécute le script suivant :

    ```python
    stock = {"kiwis": 2, "pommes": 10, "bananes": 6, "noix": 4}
    for (fruit, nombre) in stock.items():
        if nombre < 5:
            stock[fruit] = 0
    print(stock)

    ```


    === "Cocher la ou les affirmations correctes"
        
        - [ ] `{"pommes": 10, "kiwis": 0, "bananes": 6, "noix": 0}`
        - [ ] `{"kiwis": 0, "pommes": 10, "bananes": 6, "noix": 0}`
        - [ ] `{"0": 2, "pommes": 10, "bananes": 6, "0": 4}`
        - [ ] `{"kiwis": 2, "pommes": 10, "bananes": 6, "noix": 4}`

    === "Solution"
        
        - :white_check_mark: `{"pommes": 10, "kiwis": 0, "bananes": 6, "noix": 0}` Il n'y a pas d'ordre pour écrire un dictionnaire.
        - :white_check_mark: `{"kiwis": 0, "pommes": 10, "bananes": 6, "noix": 0}`
        - :x: ~~`{"0": 2, "pommes": 10, "bananes": 6, "0": 4}`~~
        - :x: ~~`{"kiwis": 2, "pommes": 10, "bananes": 6, "noix": 4}`~~ Il faut modifier les valeurs strictement inférieures à 5 en 0.

???+ question

    `couleurs = {"white": "blanc", "black": "noir", "red": "rouge"}`

    Que renvoie l'expression `couleurs[0]` ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `"white"`
        - [ ] `"blanc"`
        - [ ] Une erreur
        

    === "Solution"
        
        - :x: ~~`"white"`~~
        - :x: ~~`"blanc"`~~
        - :white_check_mark: Une erreur. On accède à une valeur avec la clé
         

???+ question

    `couleurs = {"white": "blanc", "black": "noir", "red": "rouge"}`

    Quelle instruction faut-il écrire pour modifier ce dictionnaire et obtenir :  
    `couleurs = {"white": "blanc", "black": "noir", "red": "rouge, "green": "vert"}`

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `couleurs["green"] = "vert"`
        - [ ] `couleurs.append("green", "vert")`
        - [ ] Ce n'est pas possible
     
    === "Solution"
        
        - :white_check_mark: `couleurs["green"] = "vert"`
        - :x: ~~`couleurs.append("green", "vert")`~~
        - :x: ~~Ce n'est pas possible~~

???+ question

    `stock = {"kiwis": 2, "pommes": 10, "bananes": 6, "noix": 4}`  
    Quelle est l'instruction qui permet d'afficher le plus grand nombre de fruits d'une variété ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `print(max(stock))`
        - [ ] `print(max(stock.values()))`
        - [ ] `print(max(stock.values))`
        - [ ] `print(max([stock[fruit] for fruit in stock]))`
     
    === "Solution"
        
        - :x: ~~`print(max(stock))`~~ 
        - :white_check_mark: `print(max(stock.values()))`
        - :x: ~~`print(max(stock.values))`~~ Ne pas oublier les parenthèses
        - :white_check_mark: `print(max([stock[fruit] for fruit in stock]))`
    
        
😊 Si vous avez bien réussi ces QCM, vous pouvez passer aux exercices dans la section suivante ...
