##### MODULE GUI FERME DE GASTON #####
##### auteur : JL Thirot - Vitré #####
#####           version 0.1      #####

import tkinter as tk
from tkinter.messagebox import showinfo          # Les alertes
from gestion_stock_2023 import *



def refresh_menu():
    global animal_box
    # Reset var and delete all old options
    animal.set('')
    animal_box['menu'].delete(0, 'end')

    # Insert list of new options (tk._setit hooks them up to var)
    new_choices = ('one', 'two', 'three')
    for choice in ferme.keys():
        print("ajoute",choice)
        animal_box['menu'].add_command(label=choice, command=tk._setit(animal, choice))
    animal.set(list(ferme.keys())[0])


def setLstBox():
    # placement des entrée de la liste (clef du dico)
    print("mise à jour de la liste des animaux dans le menu  option ventes")
    lstbox = ""
    for cle in ferme :
        lstbox += cle+ "\ (" + str(ferme[cle])+ ") "
    texte.set(lstbox)
    lst = setAnimalBox()

def setInfo(ferme):
    print("affichage des infos")
    txt1.set("Nombre d'espèces présentes dans votre ferme "+str(nb_especes(ferme)))
    txt2.set("Vous avez : "+ especes(ferme))
    txt3.set("Vous avez : "+ str(nb_total_animaux(ferme))+" animaux")

def fct_vente():

    if quantite.get()=="" :
        err_quantite("vendre")
        return

    q = int(quantite.get())
    a = animal.get()
    print("Vous voulez vendre",q,a+"(s)")
    if achat_vente(-q,a,ferme) == 0 :
        setLstBox()
        setInfo(ferme)
    else :
        stock()
    refresh_menu()

def fct_achat():
    global ferme

    if animal_a.get()=="" :
        choix_a()
        return

    if quantite_a.get()=="" :
        err_quantite("acheter")
        return


    q = int(quantite_a.get())
    a = animal_a.get()
    print("Vous voulez acheter",q,a+"(s)")
    achat_vente(q,a,ferme)
    setLstBox()
    setInfo(ferme)
    refresh_menu()

def err_quantite(a_v):
    showinfo("Attention","Vous devez renseigner la quantité que vous voulez "+a_v)

def choix_a():
    showinfo("Attention","Vous devez renseigner le nom de l'animal que vous voulez acheter ")

def stock():
    showinfo("Attention","Votre stock est insuffisant ! ")

def setAnimalBox() :
    anibox = []
    for cle in ferme :
        anibox+=[cle]
    anibox = tuple(anibox)
    if len(anibox)>0 :
        animal.set(anibox[0])
    else :
        animal.set(0)

    lst_ani = list(ferme.keys())
    return anibox


# Construction de la fenêtre principale «root»
root = tk.Tk()
root.title('Ferme de Gaston')
root.geometry("628x395")
root.configure(bg='black')

ferme = init_dict()
if len(ferme)==0 : ferme={"    ":""}
# variables de l'interface :
texte = tk.StringVar()          # pour la listBox de la ferme
animal = tk.StringVar(root)     # nom pour la vente
animal_a = tk.StringVar()       # nom pour l'achat
quantite = tk.StringVar()       # pour la vente
quantite_a = tk.StringVar()     # pour l'achat
txt1 = tk.StringVar()           # nb d'epsèces différentes
txt2 = tk.StringVar()           # listes des espèces
txt3 = tk.StringVar()           # nb d'animaux (total)
nb_total = tk.StringVar()       # nb total d'animaux


lst_ani = setAnimalBox()




########################
#     cadre ferme      #
########################

color_ferme="ivory"

# cadre pour la ferme :
frame_ferme = tk.Frame(root,bg=color_ferme,bd=2,relief="sunken")
frame_ferme.grid(column=0,row=0,pady=0)
# label Ferme
lab_ferme = tk.Label(frame_ferme,text="Votre ferme",bg=color_ferme,font= ('Helvetica', '16') )
lab_ferme.grid(column=0,row=0,columnspan=2)

# listbox de toute la ferme acuelle :
liste_box = tk.Listbox(frame_ferme, listvariable=texte, height=6)
liste_box.grid(column=0,row=1,padx=15,pady=8)

setLstBox()

########################
#     cadre vendre     #
########################
color_v="ivory"

# cadre pur la vente :
vente = tk.Frame(root,bg=color_v,bd=2,relief="sunken")
vente.grid(column=1,row=0)
vente.columnconfigure( 1,minsize=120 )

# label (Titre)
lab_vente = tk.Label(vente,text="Vente",bg=color_v,font= ('Helvetica', '16') )
lab_vente.grid(column=0,row=0,columnspan=2)

# label animal
lab_ani = tk.Label(vente,text="Animal:",bg=color_v)
lab_ani.grid(column=0,row=1,padx=5,pady=15)

# menuOption pour choix de l'animal a vendre :
animal_box = tk.OptionMenu(vente, animal, *lst_ani)
animal_box.grid(column=1,row=1,padx=5,pady=5)
refresh_menu()


# label quantité
lab_q = tk.Label(vente,text="Quantité:",bg=color_v)
lab_q.grid(column=0,row=2,padx=5,pady=5)

# Entry de sélection de quantité
entry_q = tk.Entry(vente, textvariable=quantite,width=6)
entry_q.grid(column=1,row=2,pady=10)

# bouton vendre -> appel fct_vente()
vb = tk.Button(vente, text='Vendre', command=fct_vente)
vb.grid(column=0,columnspan=2, row=3,padx=5,pady=5,sticky='nesw')

########################
#     cadre achat      #
########################
color_a="ivory"

# cadre pour Achat
achat = tk.Frame(root,bg=color_a,bd=2,relief="sunken")
achat.grid(column=2,row=0)
achat.columnconfigure( 1,minsize=150 )

# label (Titre)
lab_achat = tk.Label(achat,text="Achat",bg=color_a,font= ('Helvetica', '16') )
lab_achat.grid(column=0,row=0,columnspan=2)

# label choix animal
lab_ani_a = tk.Label(achat,text="Animal:",bg=color_a)
lab_ani_a.grid(column=0,row=1,padx=5,pady=15)

# entry de sélection de l'animal
animal_box_a = tk.Entry(achat, textvariable=animal_a,width=15)
animal_box_a.grid(column=1,row=1,padx=5,pady=5)

# label quantité
lab_q_a = tk.Label(achat,text="Quantité:",bg=color_a)
lab_q_a.grid(column=0,row=2,padx=5,pady=5)

# Entry de sélection de quantité
entry_q_a = tk.Entry(achat, textvariable=quantite_a,width=6)
entry_q_a.grid(column=1,row=2,pady=10)

# Construction d'un  bouton achat
ab = tk.Button(achat, text = 'Acheter', width=15,command = fct_achat)
ab.grid(column=0,columnspan=2, row=3,padx=5,pady=5,sticky='nesw')

#############################
# Cadre infos sur la ferme  #
#############################

info_ferme = tk.Frame(root,bg=color_a,bd=2,relief="sunken")
info_ferme.grid(row=3,columnspan=4,padx=5,pady=8,sticky='nesw')

# label titre
lab_info = tk.Label(info_ferme,text="Information",bg=color_a,font= ('Helvetica', '16') )
lab_info.grid(column=0,row=0,columnspan=2)

# label texte1
texte1 = tk.Label(info_ferme,textvariable=txt1,bg=color_a,font= ('Helvetica', '12') )
texte1.grid(column=0,row=1,columnspan=2)

# label texte2
texte2 = tk.Label(info_ferme,textvariable=txt2,bg=color_a,font= ('Helvetica', '12') )
texte2.grid(column=0,row=2,columnspan=2,sticky="nsw")

# label texte3
texte3 = tk.Label(info_ferme,textvariable=txt3,bg=color_a,font= ('Helvetica', '12') )
texte3.grid(column=0,row=3,columnspan=2,sticky="nsw")

setInfo(ferme)

# Construction d'un  bouton quit
qb = tk.Button(root, text='Quitter', command=root.quit,width=13)
qb.grid(row=4,columnspan=4,padx=5,pady=8)

# Lancement de la «boucle principale»
root.mainloop()

# détruit la fenêtre quand on quitte la boucle :
try :
    root.destroy()
except :
    pass




