def nb_animaux(ferme, animal):
    """ renvoie le nombre d'animaux animal présents dans la liste ferme
    Par exemple nb_animaux([["lapin", 5], ["vache", 7]], "vache") renvoie 7
    """
    ...

ferme_gaston_1 = [["lapin", 5], ["vache", 7], ["cochon", 2], ["cheval", 4]]
print(nb_animaux(ferme_gaston_1, "vache"))
