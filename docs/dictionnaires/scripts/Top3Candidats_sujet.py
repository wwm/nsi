def top_3_candidats(notes):
    """
    Renvoie une liste contenant les 3 meilleurs candidats
    Prend en entrée un dictionnaire de noms/notes
    """
    pass

# Jeu de tests
###############

liste_candidats = {"Candidat 7": 2, "Candidat 2": 38, "Candidat 6": 85,
                "Candidat 1" : 8, "Candidat 3" : 17, "Candidat 5" : 83, "Candidat 4" : 33}

# Tests
assert top_3_candidats(liste_candidats) == ['Candidat 6', 'Candidat 5', 'Candidat 2']
