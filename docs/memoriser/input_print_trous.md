---
author: Mireille Coilhac
title: input et print - À vous
---

## Entrées et Sorties

!!! info "Entrées et Sorties"

	Un code effectue des calculs. La plupart du temps, il a besoin de données en **entrée** (sinon il refera toujours la même chose) et il doit nous communiquer un résultat (numérique, texte, dessin ou autre...) en **sortie**.

	* On a de nombreux moyens de fournir des données au programme (fichiers de nos disques durs, clavier, souris sont des périphériques d'entrées)

	* L'écran, l'imprimante, les haut-parleurs, et les fichiers de nos disques durs sont des périphériques de sortie.

## `input()`

!!! info "Saisir une entrée"

	La fonction `input()` permet de saisir une entrée. 

	Cette fonction renvoie toujours une valeur de type ...

	Le message entre parenthèses est facultatif. Il permet à l’utilisateur de savoir ce qu’il doit saisir.

	👉 Par contre, même s’il n’y a rien à l’intérieur, `input()` doit absolument être accompagné de ... : 


!!! info "Les différentes saisies à connaître"

	```python
	# pour saisir un texte :
    ma_variable = ...
    
    # pour saisir un entier :
    ma_variable = ...
    
    # pour saisir un nombre décimal :
    ma_variable = ...
    ```

## `print()`

!!! info "Afficher"

	La fonction `print` permet d'afficher

!!! example "Exemple"

    ```pycon
    >>> ma_variable = 1
	>>> print(ma_variable)
	1
	>>> ma_variable = "Hello"
	>>> print(ma_variable)
	Hello
	>>> a = 1
	>>> b = 2
	>>> print("a vaut : ", a, " et b vaut : ", b)
	a vaut :  1  et b vaut :  2
	>>> 
	```


