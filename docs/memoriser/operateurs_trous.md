---
author: Mireille Coilhac
title: Opérateurs - À vous
---

## I. Les opérateurs

!!! info "Opérateur"

	Lorsque l'on fait une opération, on applique un **opérateur** sur un ou plusieurs **opérandes**

!!! example "`2 + 3`"

	`2 + 3` : l'opérateur est ... , signe de l'addition. Il y a 2 opérandes qui sont ... et ... .

!!! info "Opérandes"

	Lorsqu' il y a deux opérandes, on parle d’opérateurs binaires.
 
 	Les opérandes ne sont pas toujours des nombres, ce peut être des `int`, des `float` des `str` ou des `bool`, mais on fait aussi des opérations plus complexes sur d'autres objets .

## II. Opérateurs avec les nombres


|Opérations|	Symboles|	Exemples|
|:--|:--:|:--|
|addition|	`+`	|`2 + 5` donne $\hspace{5em}$|
|soustraction|	`-`	|`8 - 2` donne $\hspace{5em}$|
|multiplication	|`*`	|`6 * 7` donne $\hspace{5em}$|
|exponentiation (puissance)|	`**`|	`5 ** 3` donne $\hspace{5em}$|
|division	|`/`	|`7 / 2` donne $\hspace{5em}$|
|reste de division entière|	`%`|	`7 % 3` donne $\hspace{5em}$|
|quotient de division entière|	`//`|	`7 // 3` donne $\hspace{5em}$|

!!! info "modulo (`%`) et division entière (`//`)"

	* L'opérateur modulo donne le reste de la division euclidienne.   
	$7=2 \times 3 + 1$ donc `7 % 3` donne ...

	* L'opérateur division entière donne le quotient.  
	$7=2 \times 3 + 1$ donc `7 // 3` donne ...


!!! example "Exemples"

	17 divisé par 8 : "il y va ... fois" et il reste ... . ($17 =\hspace{2em} \times \hspace{2em} + \hspace{2em}$)  
	👉 `17 % 8` vaut ...   
	👉 `17 // 8` vaut ...  

	Alors que : `17 / 8` vaut ...

!!! info "Exponentiation"

	L'exponentiation (`**`) est aussi appelée puissance.

	L'opérateur `**` se lit puissance (c'est l'exponentiation) 

	Exemple :  `3**2` vaut 9

## III. Opérateurs avec les chaînes de caractères

|opérande1|	opérateur|	opérande2	|nom de l'opération|	exemple|	résultat|
|:--:|:--:|:--:|:--|:--|:--|
|str	|+	|str|	concaténation	|`"bon" + "jour"`|	$\hspace{15em}$|
|str|	`*`|	int|	répétition|	`"Aie" * 3`	|$\hspace{15em}$|
|int|	`*`|	str	|répétition|	`3 * "Aie"`|	$\hspace{15em}$|
|str|	in	|str|	est dans|	`"a" in "blabla"`|	$\hspace{15em}$|
|str|	in	|str|	est dans|	`"e" in "blabla"`	|$\hspace{15em}$|

