---
author: Mireille Coilhac
title: Hexadécimal - À vous
---


## I. La base 16

### 1. Les nombres en hexadécimal

!!! abstract "Résumé"

	👉 En base 16 (hexadécimal), nous disposons de 16 symboles 0, 1, 2, …, 9, A, B, C, D, E, F pour écrire les nombres.

<table>
<tr >
<td style="width:100px;">Base 10</td>
<td width="50px">1</td>
<td  width="50px">2</td>
<td width="50px">3</td>
<td  width="50px">4</td>
<td  width="50px">5</td>
<td  width="50px">6</td>
<td  width="50px">7</td>
<td  width="50px">8</td>
<td  width="50px">9</td>
<td  width="50px">10</td>
<td  width="50px">11</td>
<td  width="50px">12</td>
<td  width="50px">13</td>
<td  width="50px">14</td>
<td  width="50px">15</td>

</tr>
<tr >
<td style="width:100px;">Base 16</td>
<td width="50px">1</td>
<td  width="50px">2</td>
<td width="50px">3</td>
<td  width="50px">4</td>
<td  width="50px">5</td>
<td  width="50px">6</td>
<td  width="50px">7</td>
<td  width="50px">8</td>
<td  width="50px">9</td>
<td  width="50px">A</td>
<td  width="50px">B</td>
<td  width="50px">C</td>
<td  width="50px">D</td>
<td  width="50px">E</td>
<td  width="50px">F</td>

</tr>
</table>

!!! example "Exemple"

	Nous utilisons toujours le même principe : 
	Le nombre 2021 peut se décomposer de la façon suivante : $7 \times 16^2 + 14 \times 16^1 + 5 \times 16^0$

	$(2021)_{10} = (7E5)_{16}$


### 2. Compter en hexadécimal

<table>
<tr >
<td style="width:100px;">Base 10</td>
<td width="50px">1</td>
<td  width="50px">2</td>
<td width="50px">3</td>
<td  width="50px">4</td>
<td  width="50px">5</td>
<td  width="50px">6</td>
<td  width="50px">7</td>
<td  width="50px">8</td>
<td  width="50px">9</td>
<td  width="50px">10</td>
<td  width="50px">11</td>
<td  width="50px">12</td>
<td  width="50px">13</td>
<td  width="50px">14</td>
<td  width="50px">15</td>
<td  width="50px">16</td>
<td  width="50px">17</td>
<td  width="50px">18</td>
<td  width="50px">19</td>
<td  width="50px">20</td>
</tr>

<tr >
<td style="width:100px;">Base 16</td>
<td width="50px">1</td>
<td  width="50px">2</td>
<td width="50px">3</td>
<td  width="50px">4</td>
<td  width="50px">5</td>
<td  width="50px">6</td>
<td  width="50px">7</td>
<td  width="50px">8</td>
<td  width="50px">9</td>
<td  width="50px">A</td>
<td  width="50px">B</td>
<td  width="50px">C</td>
<td  width="50px">D</td>
<td  width="50px">E</td>
<td  width="50px">F</td>
<td  width="50px">10</td>
<td  width="50px">11</td>
<td  width="50px">12</td>
<td  width="50px">13</td>
<td  width="50px">14</td>
</tr>

</table>

???+ question

    Vous avez compris le principe. Ecrivez les 20 entiers écrits en hexadécimal qui suivent 21 qui est écrit en hexadécimal.

    ??? success "Solution"

        22 - 23 - 24 - 25 - 26 - 27 - 28 - 29 - 2A - 2B - 2C - 2D - 2E - 2F - 30 - 31 - 32 - 33 - 34 - 35

???+ question

    Quel est l'entier écrit en hexadécimal qui suit le nombre FF écrit en hexadécimal ?

    ??? success "Solution"

        100

???+ question

    Quel est l'entier écrit en hexadécimal qui suit le nombre FFF écrit en hexadécimal ?

    ??? success "Solution"

        1000

???+ question

    Quel est l'entier écrit en hexadécimal qui suit le nombre 2FFF écrit en hexadécimal ?

    ??? success "Solution"

        3000

??? note pliée "Utilisation du binaire et de l'hexadécimal"

	Vous l'aurez remarqué, les nombres en base 2 s'écrivent avec beaucoup de chiffres.
	Si cela répond bien à la structure interne de la mémoire d'un ordinateur, cela les rend difficiles à lire pour les humains.

	😀 Pour cette raison, on utilise fréquemment la base 16. Nous l'avons déjà vu pour le codage des couleurs.


## III. Les conversions

### 1. De la base 16 à la base 10


!!! example "Exemple : de la base 16 à la base 10"

	 $(80C)_{16} = $ ...


### 2. De la base 10 à la base 16

En fait le principe est le même que celui que nous avons déjà étudié pour passer de la base 10 à la base 2.

!!! abstract "Le principe"

	* Nous allons faire des divisions successives par 16, car nous voulons convertir notre nombre en base 16.
	* On poursuit les divisions jusqu'à obtenir un quotient égal à **0** pour la dernière division.
	* On lit ensuite les restes, en partant du bas.	

!!! example "Etudier cet exemple"

	Source : [Académie de Limoges](http://pedagogie.ac-limoges.fr/sti_si/accueil/FichesConnaissances/Sequence2SSi/co/ConvDecimalHexadecimal.html){:target="_blank" }

	![divisions](images/conv_dec_hexa.png){ width=60% }


???+ question "Conversion"

    $(1256)_{10}  = (?)_{16 }$ 

    ??? success "Solution"

    	4E8


### 3. En base 16 : les groupements par 4 bits

Remarquons que $(2^4)_{10} = (16)_{10}=(10000)_2$

Les chiffres de 0 à 15 seront donc codés sur 4 bits en binaire, et sur un seul (1, 2 ... F) en hexadécimal.

De même,  $(10)_{16} = (16)_{10} = (2^4)_{10} = (0001\ 0000)_2$

De même $(FF)_{16} = 15 \times 16^1 + 15 = (255) _{10} = (1111\ 1111)_2$

&#128073;On constate que le 1er groupe de 4 bits code le premier chiffre en hexadécimal, et que le second groupe de 4 bits code le second chiffre hexadécimal.

Cette propriété reste vraie pour de plus grands nombres. 

!!! example "Etudier cet exemple"

	$(4E5)_{16} = $ ...
	
	En effet : 

	* $(4)_{16}  = $...
	* $(E)_{16}  = $...
	* $(5)_{16}  = $...

!!! abstract "Le principe"

	Pour convertir de la base 2 vers la base 16, **ou inversement**, il suffit de faire 

!!! example "Exemple"

	![D6](images/D6.jpg){ width=30% }

### 4. 😀 Conversion directe entre l'hexadécimal et le binaire


!!! example "Exemple"

	On écrit chaque chiffre en binaire sur 4 bit.


	$(AC1)_{16} = (?)_2$

	$(A)_{16} =(10)_{10}=$...

	$(C)_{16} =(12)_{10}=$...

	$(1)_{16} =(1)_{10}=$...

	![AC1](images/AC1.jpg){ width=30% }

	et donc : $(AC1)_{16} =$...





