---
author: Mireille Coilhac
title: if elif else - Bilan
---


!!! danger "Attention à ne pas oublier les deux points et l'indentation."


!!! abstract "La syntaxe de if :"

    👉 Attention, l'instructions `else` n'est pas du tout obligatoire.  
	
	```
    # Avec seulement if
	if condition :
   		bloc d'instructions à réaliser
   	...
   	```

   	```
    # Avec if et else
	if condition :
   		bloc d'instructions à réaliser
   	else:
   		autre bloc d'instructions à réaliser
   	...
   	```


!!! abstract "La syntaxe de if ... elif ... else :"

    👉 Attention, l'instructions `else` n'est pas du tout obligatoire.  
	
	```
    # Avec seulement if et elif
	if condition :
   		bloc d'instructions à réaliser
   	elif autre condition:
   		autre bloc d'instructions à réaliser
   	...
   	```

	```
    # Avec if ... elif ... else
	if condition :
   		bloc d'instructions à réaliser
   	elif autre condition:
   		autre bloc d'instructions à réaliser
   	else:
        encore un autre bloc d'instructions à réaliser
   	...
   	```