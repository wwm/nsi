---
author: Mireille Coilhac
title: Compléments
---

😊 Pour approfondir : [Interstices](https://interstices.info/les-algorithmes-de-tri/){ .md-button target="_blank" rel="noopener" }
