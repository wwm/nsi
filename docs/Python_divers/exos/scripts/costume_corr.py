def costume(rang, couleurs):
    n = len(couleurs)
    indice = (rang - 1) % n
    return couleurs[indice]
