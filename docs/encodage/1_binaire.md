---
author: Mireille Coilhac
title: Cours/TD Binaire
---

## I. Préambule (à méditer)

### 1. Entiers et flottants

Pourquoi fait-on la différence en Python entre integer et float?

Regardons deux exemples :

🌵 Résoudre l'équation $x+1=x$

!!! example "Tester en console"

    Dans la console saisir à la main sans "copier/coller" :

    ```pycon
    >>> 2**53 + 1 == 2**53
    >>> 2.0**53 + 1.0 == 2.0**53
    ```

    {{ terminal() }}


!!! example "Il y a là quelques chose à comprendre non ?"

	Mais ça va prendre quelques … minutes...

	🌵 Dans la console de Python ci-dessous, tester `2023**2023`, puis `2023.0**2023.0`

	{{ terminal() }}

!!! info "A savoir"

	👉 2023 est de type **entier**, 2023.0 est de type **flottant**

	Nous allons commencer par étudier les entiers puis nous comprendrons ces différences de comportements.


### 2. Les différentes bases

Regarder sur ce site : [🌐 Les couleurs](https://www.toutes-les-couleurs.com/code-couleur-html.php){:target="_blank" } , comment sont codées les couleurs en html (pour les sites web).



## II. La numération des shadoks

<iframe width="560" height="315" src="https://www.youtube.com/embed/lP9PaDs2xgQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

???+ question "Les Shadocs"

    En quelle base comptent les shadoks ?

    ??? success "Solution"

        En base 4 !

## III. Prérequis sur les puissances

![puissances](images/regles_puissances.jpg){ width=15%; : .center }



{{ multi_qcm(
    [
        "Compléter : $2^3=$",
        [
            "9",
            "8",
            "Je ne sais pas",
            "6",
        ],
        [2],
    ],
    [
        "Compléter : $3^2=$",
        [
            "9",
            "8",
            "Je ne sais pas",
            "6",
        ],
        [1],
    ],
    [
        "Compléter : $2^0=$",
        [
            "0",
            "1",
            "Impossible",
            "2",
        ],
        [2],
    ],
    [
        "Compléter : $2^1=$",
        [
            "0",
            "1",
            "Je ne sais pas",
            "2",
        ],
        [4],
    ],
    [
        "Compléter : $0^3=$",
        [
            "0",
            "1",
            "Impossible",
            "3",
        ],
        [1],
    ],
    [
        "Compléter : $16^3 \\times 16^2=$",
        [
            "$16^5$",
            "$16^6$",
            "Je ne sais pas",
            "2",
        ],
        [1],
    ],
    [
        "Compléter : $16^0\\times 16^2=$",
        [
            "$16^0$",
            "$16^2$",
            "Je ne sais pas",
            "16",
            "1",
        ],
        [2],
    ],
    [
        "Compléter : $2^3+2^4=$",
        [
            "$2^7$",
            "$2^{12}$",
            "Je ne sais pas",
            "24",
        ],
        [4],
    ],
    multi = False,
    qcm_title = "QCM à refaire tant que nécessaire ...",
    DEBUG = False,
    shuffle = True,
    ) }}


## IV. Les bases numériques

### 1. un peu d'histoire

![Mayas](images/mayas.png){ width=50%; : .center }

Le mot calcul trouve son origine dans le mot latin « calculis » qui signifie « caillou ». Il y a des millénaires, il était coutume d’utiliser des cailloux pour représenter un nombre ; pour dénombrer, compter.

Les représentations écrites d’un nombre sont multiples ; elles varient selon les cultures. On distingue principalement 2 types d’écritures :

!!! info "Celles à base de symboles qu’il faut additionner (et parfois soustraire) entre eux"

	Par exemple :

	🌴 La numération mésopotamienne, 3000 ans av J.C. :

	![Mesopotamie](images/numeration_mesopotamienne.jpg){ width=50%; : .center }

	Source de l'image : [🌐 wikipedia](https://fr.wikipedia.org/wiki/Num%C3%A9ration_m%C3%A9sopotamienne){:target="_blank" }


	🌴 La numération romaine : MMXXIII = 2023

!!! info "Celles à base de rang (ou de position)"

	C’est celle que nous utilisons actuellement tous les jours :

	$2023 = 2 \times 1000 + 0 \times 100 + 2 \times 10 + 3 \times 1$.

	Ou encore : 2023 est égal à 2 **milliers**, 0 **centaines**, 2 **dizaines**, 3 **unités**.


!!! info "Différentes bases"

	👉 Ces représentations utilisent des bases. Pour la numération mésopotamienne c’est une base 60. C’est aussi le cas pour les heures : nous utilisons une base de 60 car 1 min = 60 s et 1 h= 60 min

	👉 Actuellement, c’est la base 10 qui est utilisée (nous avons 10 doigts !).

	👉 Les machines à calculer électronique ne connaissent pas (de manière « naturelle ») le système numérique en base 10. Elles ne savent dénombrer que 2 éléments : 0 et 1. la base utilisée est donc la base 2, on parle aussi de "binaire".

	😊 Pour pouvoir les utiliser nous devons donc savoir passer d’une base à une autre.

	👉 Nous allons voir qu’il existe un principe général et utiliserons régulièrement les bases 2,10 et 16.


### 2. 💡 Le principe


Quelle que soit la façon de l'écrire, et quelle que soit l'opération que l'on fait, on ne fait au fond que toujours une 
seule et même chose : compter les unités, les dizaines , les centaines etc...


$123$ c'est en fait $1$ **centaine** + $2$ **dizaines** + $3$ **unites**

Mais les mots cachent une réalité encore plus simple, c'est $1 \times 10^2 + 2 \times 10^1 + 3 \times 10^0$

Mieux encore ! Supposons que nous comptions en base 5 : 


<table>
<tr >
<td style="width:100px;">Nombre en base 10</td>
<td width="50px">1</td>
<td  width="50px">2</td>
<td width="50px">3</td>
<td  width="50px">4</td>
<td  width="50px">5</td>
<td  width="50px">6</td>
<td  width="50px">7</td>
<td  width="50px">8</td>
<td  width="50px">9</td>
<td  width="50px">10</td>
<td  width="50px">11</td>
</tr>
<tr >
<td style="width:100px;">Nombre en base 5</td>
<td width="50px">1</td>
<td  width="50px">2</td>
<td width="50px">3</td>
<td  width="50px">4</td>
<td  width="50px">10</td>
<td  width="50px">11</td>
<td  width="50px">12</td>
<td  width="50px">13</td>
<td  width="50px">14</td>
<td  width="50px">20</td>
<td  width="50px">21</td>
</tr>
</td>
</table>


$(21)_5 = 2 \times 5^1 + 1 \times 5^0 = (11)_{10}$


Et on n'a même pas besoin de connaître les tables de multiplication, juste savoir compter et poser des retenues. La multiplication européenne, plus compacte, demande quand à elle la connaissance des tables jusqu'à $10$. 

Au delà... on ne fait finalement qu'additionner les unités, les dizaines (ou « cinquaines » etc...)

???+ question "Base 12 ? base 60 ?"

    Et ou trouve-t-on encore de nos jours, couramment, des comptes en base 12 ? en base 60 ?

    ??? success "Solution"

         * Pour la base 12 : les heures
         * Pour la base 60 : les minutes

### 3. Lire un nombre

!!! example "un exemple en base 10"

	$12734 = 4 \times 10^0 + 3 \times 10^1 + 7 \times 10^2 + 2 \times 10^3 + 1 \times 10^4$


!!! example "un exemple en base 2"

	$(101010)_2 = 0 \times 2^0 + 1 \times 2^1 + 0 \times 2^2 + 1 \times 2^3 + 0 \times 2^4 + 1 \times 2^5 = 0 + 2 + 0+ 8 + 0 + 32  =  (42)_{10}$


!!! example "Alors en base 5 ? C'est pareil !"

	$(12334)_5 = 4 \times 5^0 + 3 \times 5^1 + 3 \times 5^2 + 2 \times 5^3 + 1 \times 5^4 = 4 + 15 + 75 + 250 + 625  =  (969)_{10}$


😊 Pas si difficile ! Vous avez appris à convertir un nombre d'une base N vers la base 10.


!!! info "Notations"

	Pour préciser dans quelle base sont écrits les nombres, on peut utiliser des parenthèses : 

	$(42)_{10}=(101010)_2$

	Autres notations possibles :$(101010)_2 = 101010 _{(2)}= 0\text{b}101010 = 101010\text{b}$


### 4. Compter en binaire

!!! example "Quelques exemples"

	$(1)_2+(1)_2=(2^0  + 2^0)_{10}=(2 \times 2^0)_{10} =(2^1)_{10}= (10)_2$

	$(10)_2 +(1)_2=(2^1+2^0)_{10}=(11)_2$

	$(11)_2+(1)_2= (2^1+2^0+2^0)_{10}=  (2^1+2 \times 2^0)_{10}=  (2^1+2^1)_{10}=(2 \times 2^1)_{10}=(2^2)_{10}=(100)_2$


!!! info "Comptons en base 2"

	Compter, c'est ... ajouter 1 😊 

	0; 1; 10; 11; 100; 101; 110; ...


???+ question "Si on comptait ?"

    Trouver les entiers qui suivent ceux-ci lorsque l'on compte : 0; 1; 10; 11; 100; 101; 110;

    ??? success "Solution"

    	111; 1000; 1001; 1010; 1011; 1100; 1101; 1110; 1111; 10000; 10001; 10010 ...


???+ note dépliée "Remarquer en décimal"

	* 99 + 1 = 100 
	* 999 + 1 = 1000
	* 9999 + 1 = 10000 …

### 5. De la base 10 vers la base 2

!!! info "Poser les divisions _à la main_"

	Petit retour sur les divisions dites "euclidiennes"

	![div_eucl](images/div_eucl.png){ width=20%; : .center }

	> Source de l'image : [🌐 Division euclidienne](https://www.jeuxmaths.fr/cours/division-euclidienne.php){:target="_blank" }


!!! info "Méthode par divisions successives"

	🤔 Etudier l'exemple ci-dessous

	👉 On poursuit les divisions jusqu'à obtenir un quotient **égal à 0** pour la dernière division.  
	On lit ensuite les restes, en partant du bas.

	![decimal_binaire](images/decimal_binaire.png){ width=30%; : .center }

	> Source de l'image : [🌐 Académie de Limoges](http://pedagogie.ac-limoges.fr/sti_si/accueil/FichesConnaissances/Sequence2SSi/co/ConvDecimalBinaire.html){:target="_blank" }



## V. Exercices

???+ question "Exercice 1"

    1110010 est écrit en base 2. L'écrire en base 10.

    ??? success "Solution"

		$(1110010)_2 = 0 \times 2^0 + 1 \times 2^1 + 0 \times 2^2 + 0 \times 2^3 + 1\times 2^4 + 1 \times 2^5 + 1 \times 2^6 = 2 + 16 + 32 + 64  =  (114)_{10}$

    	1110010 en binaire s'écrit 114 en décimal.



???+ question "Exercice 2"

    convertir 23 écrit en décimal en binaire

    ??? success "Solution"

    	10111

    	![23](images/div_succ.jpg){ width=30%; : .center }


???+ question "Exercice 3"

    234 est écrit en base 10. L'écrire en binaire

    ??? success "Solution"

    	Le nombre 234 écrit en base dix, s'écrit 11101010 en base deux .



???+ question "Exercice 4"

	On peut aussi convertir un nombre décimal en binaire en utilisant un tableau.

	[Utiliser un tableau](https://wims.math.cnrs.fr/wims/wims.cgi?lang=fr&cmd=new&module=H3%2Fcoding%2Foefbin.fr&exo=binary&qnum=1&scoredelay=&seedrepeat=0&qcmlevel=1&special_parm2=&special_parm4=){ .md-button target="_blank" rel="noopener" }

	

???+ question "Exercice 5 : A vous de jouer !"

	Imaginez vos propres exercices, et vérifiez la réponse ci-dessous.

	[Convertisseur WIMS](https://wims.math.cnrs.fr/wims/wims.cgi?module=tool/number/baseconv.en){ .md-button target="_blank" rel="noopener" }


## VI. Les instructions Python

!!! info "Tester en console"

    Dans la console saisir à la main sans "copier/coller" :

    ```pycon
    >>> bin(42)
    >>> int(0b101010)
    >>> int("101010", 2)  # (1)
    ```

    1. "101010" est de type str. 2 est la base dans lequel il est écrit. On obtient une conversion de ce nombre en décimal.

    {{ terminal() }}

???+ question "Types"

	Quels sont les types de 

	* `bin(42)`
	* `int(0b101010)`
	* `int("101010", 2)`

	{{ terminal() }}


??? success "Solution"

	En utilisant la fonction `type` on obtient :
	```pycon
	>>> type(bin(42))
	<class 'str'>
	>>> type(int(0b101010))
	<class 'int'>
	>>> type(int("101010", 2))
	<class 'int'>
	>>> 
	```

## VII. Des opérations en binaire

### 1. Les additions

!!! info "Rappel en décimal"

    ![addition décimale](images/addition_dec.png){ width=20% }

!!! example "Exemples en binaire"

    $1100110_2+1111101_2 = ?$

    ![addition 1](images/addition_1.png){ width=30% }

    $1111111_2+1_2 = ?$

    ![addition 2](images/add_bin.jpg){ width=30% }

    On a donc $1111111_2+1_2 =  100000000_2$

	👉 C'est important à remarquer, et cela se généralise évidemment pour tous les nombres constitués uniquement de 1 auxquels on ajoute 1.





### 2. Les multiplications

!!! info "Multiplier par 10 en décimal"

	En base  10 , multiplier par  10  c'est ajouter un 0 à la fin : $134 \times 10 = 1340$

	C'est facile à comprendre : 

	$134 \times 10 =(1 \times 10^2 + 3  \times10^1 + 4 \times 10^0) \times 10$  
	$=(1 \times 10^3 + 3  \times10^2 + 4 \times 10^1+0 \times 10^0)$  
	Ce résultat s'écrit bien $(1340)_{10}$

!!! info "Multiplier par 2 en binaire"

	Multiplier par 2 en binaire se fait de façon analogue à multiplier par 10 en décimal : il suffit d'ajouter un 0 à la fin.

	C'est facile à comprendre : 

	$(1011)_2 \times (2)_{10}$
	
	$= (1 \times 2^3 + 0  \times2^2 + 1 \times 2^1+1 \times 2^0)_{10} \times (2)_{10}$  

	$=(1 \times 2^4 + 0  \times2^3 + 1 \times 2^2+1 \times 2^1+0 \times 2^0)_{10}$  

	Ce résultat s'écrit bien $(10110)_{2}$

???+ note dépliée "Remarque"

    $(1011)_2 \times (2)_{10}=(1011)_2 \times (10)_2$

    On peut poser l'opération de façon analogue à ce que l'on fait en décimal :

    $\begin{array}{*{20}{c}}
	{}&1&0&1&1\\
 	\times &{}&{}&1&0\\
	\hline
	{}&0&0&0&0\\
	1&0&1&1&{}\\
	\hline
	1&0&1&1&0
	\end{array}$

	😊 C'est facile à comprendre : 

	$(1011)_2 \times (2)_{10}= (1 \times 2^3 + 0  \times2^2 + 1 \times 2^1+1 \times 2^0)_{10} \times (2)_{10}$

	$=(1 \times 2^4 + 0  \times2^3 + 1 \times 2^2+1 \times 2^1+0 \times 2^0)_{10}$

	Ce résultat s'écrit bien $(10110)_{2}$


!!! info "💚 A noter"

    Quand on multiplie en binaire par **2** on écrit un **0** à la droite du nombre, comme pour la multiplication par **10** en décimal.



## VIII. Les bits et les octets

!!! info "Les bits"

	Le bit est l'unité élémentaire d'information : **0** ou **1**. 

	Avec un seul bit on peut donc faire 2 "mots"

	* **0**
	* **1**

	Avec 2 bits on pourra faire 4 "mots": 

	* **00**
	* **01**
	* **10**
	* **11**

	Avec 3 bits on pourra faire 8 "mots": 

	* **000**
	* **001**
	* **010**
	* **011**
	* **100**
	* **101**
	* **110**
	* **111**

	Et ainsi de suite, à chaque fois qu'on ajoute un bit, on multiplie par deux le nombre de "mots" possibles.
 
???+ question "Exercice 1"

    Pour coder tous les nombres entiers de 1 à 1000, combien de bits faut-il ?

    ??? success "Solution"

        $(1000)_{10}=(1111101000)_2$

        Il faut donc 10 bits.

!!! info "Les octets"

	Dans la mémoire de l'ordinateur les informations sont codées dans des octets. C'est la plus petite unité d'information qu'on peut lire/écrire. Un octet correspond à 8 bits.

	👉 Un octet permet donc de coder $2^8=256$ valeurs différentes.


???+ note dépliée "Les pixels dans les images"

    En SNT vous avez appris que les couleurs sont codée avec 3 nombres allant de 0 à 255.

	🌵 Notez bien qu'il y a bien 256 valeurs différentes entre 0 et 255

	Autrement dit, chacun de ces trois nombres est codé sur 1 octet, c'est-à-dire sur 8 bits.  
	L'encodage d'une couleur occupera donc 3 octets. 

	Une image de $800 \times 600$ pixels occupera dans la mémoire $800 \times 600 \times 3= 1440000 \text{ octets}=1440 \text{ koctets}=1.44 \text{ Mo}$ 

???+ question "QCM"

	👉 Si vous n'avez pas d'identifiant, saisir le nombre $0$.

	<iframe style="width:95%;margin-left:2%;height:1200px;" src="https://genumsi.inria.fr/qcm.php?h=6c3a8cd7765824fe6a1eff345e55a3ff"></iframe>

## IX. La NASA vous parle ...

<iframe width="560" height="315" src="https://www.youtube.com/embed/NpVaQvaR0hw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


<div class="centre">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/message_cache_NASA_2022_sujet.ipynb"
width="900" height="900" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

### Correction 


<div class="centre">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/message_cache_NASA_2022_corr.ipynb"
width="900" height="900" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

<!--- La correction  plus tard 
-->

## X. Une curiosité en vidéo

<iframe width="560" height="315" src="https://www.youtube.com/embed/HsS7kaFDrXg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

