---
author: jean-Louis Thirot et Mireille Coilhac
title: Exercices Binaire et Python
---

## Exercice 1

???+ question "Pour comprendre"

	Nous allons manipuler des nombres en base 10, qui seront de type `int` et des nombres écrits en binaire, qui seront de type `str`.

	Pour comprendre comment convertir en décimal un nombre écrit en binaire regardons cet exemple : 

	`entier_binaire = "1011"`

	On va parcourir la chaîne de caractères `"1011"`. Chaque caractère sera converti en `int`.  
	Il faudra ensuite faire la somme comme vu dans le cours.

	Tester ci-dessous

    {{IDE('scripts/explication')}}


## Exercice 2

???+ question "A vous de jouer"

	Le code caché affecte une chaîne de caractères correspondant à un nombre en binaire à `entier_binaire`

	Le code que vous devez complétez doit afficher le nombre décimal correspondant à `entier_binaire`

	Vous pouvez vous aider des explications de l'exercice 1.

	{{IDE('scripts/bin_to_dec')}}


??? success "Solution"

	```python
	p = len(entier_binaire) - 1
	somme = 0
	for caractere in entier_binaire :
   		chiffre = int(caractere)
   		somme = somme + chiffre * 2**p
   		p = p - 1
	print("Le nombre ", entier_binaire," en binaire, vaut ", somme, "en décimal")
	```





