---
author: Nicolas Revéret, Jean-Louis Thirot et Mireille Coilhac
title: Exercices sur les tableaux
tags:
    - liste/tableau
---

## QCM 1

???+ question "Parcours sur les indices ou sur les valeurs ?"

    On considère un `#!py tableau` **non vide** contenant des valeurs quelconques. Indiquer dans chaque cas le bon type de parcours à effectuer.

    * On souhaite déterminer **l'indice de la valeur maximale**.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :white_check_mark: Parcours sur les indices
            - :x: Parcours sur les valeurs

    * On souhaite calculer **la somme des valeurs**.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :x: Parcours sur les indices
            - :white_check_mark: Parcours sur les valeurs

    * On souhaite **créer un nouveau tableau ne contenant que les valeurs de la première moitié de `#!py tableau`**.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :white_check_mark: Parcours sur les indices
            - :x: Parcours sur les valeurs

    * On souhaite **déterminer les deux *extrema* (minimum et maximum)**.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :x: Parcours sur les indices
            - :white_check_mark: Parcours sur les valeurs

    * On souhaite **élever au carré toutes les valeurs du `#!py tableau`** en écrivant les nouvelles valeurs dans le même `#!py tableau`. Par exemple `#!py [2, 3, 4]` deviendrait `#!py [4, 9, 16]`.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :white_check_mark: Parcours sur les indices
            - :white_check_mark: Parcours sur les valeurs avec une liste en compréhension : `#!py tableau = [x * x for x in tableau]`

## Les notes d'Alice - Saison 1

![bulletin](images/notes_Alice.jpg){ width=20%; : .center }

**1.**	Alice veut créer la liste des notes qu’elle a obtenues ce trimestre en NSI.  
Ces notes ne sont pas forcément des nombres entiers.  
Ecrire une fonction qui demande de saisir les notes une par une, et qui renvoie `liste_notes`, la liste des notes qu’elle a obtenues.  

??? tip "Astuce 1"

    Vous pouvez prendre en paramètre de la fonction le nombre de notes à saisir.

??? tip "Astuce 2"

    vous pouvez initialiser `liste_notes = []`, puis ajouter dans la liste les notes une par une avec une boucle.

???+ question "Compléter le code"

    {{IDE('scripts/alice_creation_liste')}}

??? success "Solution"

    ```python
    def creation_liste(n):
        """
        n est un entier donnant la taille de la liste à saisir
        La fonction renvoie la liste saisie
        """
        liste_notes = []
        for i in range(n):
            note = float(input("saisir votre note : "))
            liste_notes.append(note)
        return liste_notes
    ```

**2.**	Créer le programme qui demande la saisie du nombre de notes, crée la liste des notes, affiche la plus petite note, et la plus haute.  
💡 Indication : tester les fonctions `min` et `max`.


???+ question "Ecrire le code ci-dessous"

    {{ IDE() }}

??? success "Solution"

    ```python
    n = int(input("nombre de notes à saisir : "))
    notes = creation_liste(n) # de type liste
    note_mini = min(notes)
    note_maxi = max(notes)
    print("note minimale :  ", note_mini)
    print("note maximale :  ", note_maxi)
    ```

**3.**	Créer une fonction `moyenne` qui prend en paramètre une liste `lst` et renvoie la moyenne des éléments de cette liste.  
  

???+ question "Compléter le code"

    Compléter le script pour qu’il affiche la moyenne des notes saisies par Alice.

    {{IDE('scripts/alice_moyenne_liste')}}

??? success "Solution"

    ```python
    def moyenne(lst):
        """
        Cette fonction renvoie la moyenne de la liste lst
        """
        somme = 0  #initialisation de la somme des notes
        for note in lst:  # calcul de la somme des notes avec une boucle
            somme = somme + note
        moyenne = somme/len(lst)  # len(liste) est le nombre de notes
        return moyenne

    n = int(input("nombre de notes à saisir : "))
    notes = creation_liste(n)
    print(notes)  
    moyenne_notes = moyenne(notes)
    print(moyenne_notes)
    ```

   
## Exercices du site "CodEx"

!!! warning "Remarque"

    Ne faire que les exercices dont les liens sont donnés ici, car certains se trouvant sur le menu de gauche nécessitent des connaissances que nous n'avons pas encore vues.


???+ question "Recherche d'indice - non guidé"

    Il s'agit de déterminer l'indice de la plus petite valeur dans un tableau non-vide.

    [Indice du minimum d'un tableau](https://codex.forge.apps.education.fr/exercices/ind_min/){ .md-button target="_blank" rel="noopener" }

???+ question "Recherche de valeur - non guidé"

    La recherche de la valeur maximale dans un tableau. Classique.
    
    [Maximum](https://codex.forge.apps.education.fr/exercices/maximum_nombres/){ .md-button target="_blank" rel="noopener" }

???+ question "Lecture dans un tableau - non guidé"

    On donne les altitudes des différentes étapes d'une course en montagne. On demande quel est le dénivelé positif total.

    [Dénivelé positif](https://codex.forge.apps.education.fr/exercices/denivele_positif/){ .md-button target="_blank" rel="noopener" }


???+ question "Comparaison d'éléments consécutifs - non guidé"

    Le tableau fourni est-il trié ?

    [Est trié ?](https://codex.forge.apps.education.fr/exercices/est_trie/){ .md-button target="_blank" rel="noopener" }


## Modification d'un tableau


Cet exercice est à réaliser pour introduire la notion "Affectations et listes" de la section suivante.

On se donne un tableau, une valeur cible et une valeur de remplacement et il faut parcourir le tableau et remplacer la cible par la nouvelle valeur.


Écrire la fonction `remplacer` prenant en argument :

* une liste d'entiers `valeurs`
* un entier `valeur_cible`
* un entier `nouvelle_valeur`

Cette fonction doit renvoyer une **nouvelle** liste contenant les mêmes valeurs que `valeurs`, dans le même ordre, sauf `valeur_cible` qui a été remplacée par `nouvelle_valeur`. 

⚠️ **La liste passée en paramètre ne doit pas être modifiée**.

???+ example "Exemples"

    ```pycon
    >>> valeurs = [3, 8, 7]
    >>> remplacer(valeurs, 3, 0)
    [0, 8, 7]
    >>> valeurs
    [3, 8, 7]
    ```

    ```pycon
    >>> valeurs = [3, 8, 3, 5]
    >>> remplacer(valeurs, 3, 0)
    [0, 8, 0, 5]
    >>> valeurs
    [3, 8, 3, 5]
    ```

    
???+ question "Compléter"

    {{ IDE('scripts/remplacer') }}




## Les notes d'Alice - Saison 2

![bulletin](images/notes_Alice.jpg){ width=20%; : .center }

???+ question "Les notes d'Alice - Saison 2"

    **1.**	Alice veut créer la liste des notes qu’elle a obtenues ce trimestre en NSI.  
    Ces notes ne sont pas forcément des nombres entiers, et il y a des coefficients qui peuvent être différents pour chaque note.  
    Ecrire une fonction `creation_liste_tuples` qui prend en paramètres `n` le nombre de notes, demande de saisir les notes une par une, 
    avec le coefficient associé à chaque note, et qui renvoie `liste_notes`, la liste des tuples (note, coefficient) .  
    Nous avons étudié les assertions. Dans cette fonction il doit y avoir une assertion qui vérifie que chaque note saisie est un réel 
    entre 0 et 20 (compris), et une assertion qui vérifie que chaque coefficient saisi est un réel positif.  

    {{ IDE('scripts/alice_cree_liste') }}

    ??? success "Solution"

        ```python title=""
        def creation_liste_tuples(n):
            """
            n est un entier donnant la taille de la liste à saisir
            La fonction renvoie la liste des tuples (note, coefficient) .
            """
            liste_tuples = []
            for i in range(n):
                note = float(input("Saisir votre note : "))
                coef = float(input("Saisir le coefficient correspondant : "))
                assert note >= 0 and note <= 20, "la note est entre 0 et 20."
                assert coef >= 0, "le coefficient est positif"
                liste_tuples.append((note, coef))
            return liste_tuples
        ```

    **2.** Ecrire un code qui demande la saisie des notes et coefficients, puis affiche la plus petite note, et la plus haute.   

    La fonction `creation_liste_tuples` n'est pas à écrire, vous pouvez l'utiliser, elle est en code caché.

    💡 Indication : Vous pourrez utiliser les fonctions `min` et `max`.

    {{ IDE('scripts/min_max_notes') }}

    ??? success "Solution"

        ```python title=""
        n = int(input("nombre de notes à saisir : "))
        liste_tuples_notes = creation_liste_tuples(n) # de type liste
        liste_notes = [elem[0] for elem in liste_tuples_notes]
        note_mini = min(liste_notes)
        note_maxi = max(liste_notes)
        print("note minimale :  ", note_mini)
        print("note maximale :  ", note_maxi)
        ```

    **3.** Créer une fonction `moyenne` qui prend en paramètre `lst_notes` une liste de tuples (note, coefficient) et qui 
    renvoie la moyenne arrondie à deux chiffres après la virgule, des notes de `lst_notes`.    


    ??? tip "Aide pour les arrondis"

        Exécuter ci-dessous : 

        {{ IDE('scripts/arrondi') }}


    {{ IDE('scripts/moyenne_coefs') }}


    **4.** Ecrire ci-dessous un seul script structuré qui rassemble toutes les questions :  

    * Saisie de la liste des tuples  
    * Affichage de la note minimale et de la note maximale   
    * Affichage de la moyenne arrondie à deux chiffres après la virgule.  

    Les fonctions `creation_liste_tuples` et `moyenne`ne sont pas à écrire, vous pouvez les utiliser, elles sont en code caché.

    {{ IDE('scripts/alice_complet') }}

    ??? success "Solution"

        ```python title=""
        n = int(input("nombre de notes à saisir : "))
        liste_tuples_notes = creation_liste_tuples(n) # de type liste
        liste_notes = [elem[0] for elem in liste_tuples_notes]
        note_mini = min(liste_notes)
        note_maxi = max(liste_notes)
        print("note minimale :  ", note_mini)
        print("note maximale :  ", note_maxi)
        resultat_moyenne = moyenne(liste_tuples_notes)
        print("La moyenne est : ", resultat_moyenne)
        ```

<!---
???+ question "Les notes d'Alice - Saison 2"

    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/notes_Alice_tuples_sujet.ipynb"
    width="900" height="900" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

    😀 La correction est arrivée ...

    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/notes_Alice_tuples_correction.ipynb"
    width="900" height="900" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

-->

<!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION

⏳ La correction viendra bientôt ...
    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/notes_Alice_tuples_correction.ipynb"
    width="900" height="900" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
 -->

## Soleil couchant
    
???+ question "Recherche de maxima relatifs - non guidé"

    Combien de bâtiments sont éclairés par le soleil couchant. Le sujet est original mais l'algorithme très classique.

    [Soleil couchant](https://codex.forge.apps.education.fr/exercices/soleil_couchant/){ .md-button target="_blank" rel="noopener" }
    

## Recherche d'indices

_Auteur : Sébastien HOARAU_


Ecrire une fonction qui renvoie la liste croissante des indices d'un certain élément dans un tableau.

Il est recommandé d'utiliser une liste en compréhension.

Écrire une fonction `indices` qui prend en paramètres un entier `element` et un tableau `entiers` d'entiers. Cette fonction renvoie la liste croissante des indices de `element` dans le tableau `entiers`.

Cette liste sera donc vide `[]` si `element` n'apparait pas dans `entiers`.

> On n'utilisera ni la méthode `index`, ni la méthode `max`.

???+ example "Exemples"

    ```pycon
    >>> indices(3, [3, 2, 1, 3, 2, 1])
    [0, 3]
    >>> indices(4, [1, 2, 3])
    []
    >>> indices(10, [2, 10, 3, 10, 4, 10, 5])
    [1, 3, 5]
    ```

???+ question "Compléter ci-dessous"

    {{ IDE('scripts/recherche_indices', SANS="index, max") }}
    

_Auteurs des exercices: Nicolas Revéret, Jean-Louis Thirot, Sébastien Hoareau et Mireille Coilhac_
