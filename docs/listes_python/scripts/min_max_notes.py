# --- PYODIDE:env --- #

def creation_liste_tuples(n):
    """
    n est un entier donnant la taille de la liste à saisir
    La fonction renvoie la liste des tuples (note, coefficient) .
    """
    liste_tuples = []
    for i in range(n):
        note = float(input("Saisir votre note : "))
        coef = float(input("Saisir le coefficient correspondant : "))
        assert note >= 0 and note <=20, "la note est entre 0 et 20."
        assert coef >= 0, "le coefficient est positif"
        liste_tuples.append((note, coef))
    return liste_tuples

# --- PYODIDE:code --- #

...