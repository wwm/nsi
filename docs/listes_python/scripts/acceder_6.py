m = [[1, 3, 4], [5, 6, 8], [2, 1, 3], [7, 8, 15]]
deuxieme_ligne = m[1]
print("deuxieme_ligne = ", deuxieme_ligne)
print("deuxieme_ligne[1] = ", deuxieme_ligne[1])

print("Mais comme deuxieme_ligne et m[1] c'est la même chose... :")

print("m[1][1] = ", m[1][1])
