def donne_colonne(matrice, n_colonne) -> list :
    """
    Entrée : matrice : une liste de listes, un entier n_colonne le numéro de la colonne
    Sortie : colonne : une liste dont les éléments sont les éléments de la colonne numéro n_colonne
    """
    ...


# Tests
m = [[1, 3, 4],
     [5, 6, 8],
     [2, 1, 3],
     [7, 8, 15]]
assert donne_colonne(m, 0) == [1, 5, 2, 7]
assert donne_colonne(m, 2) == [4, 8, 3, 15]
