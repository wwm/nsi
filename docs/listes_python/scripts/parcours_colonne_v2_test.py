# Tests
m = [[1, 3, 4],
     [5, 6, 8],
     [2, 1, 3],
     [7, 8, 15]]
assert donne_colonne(m, 0) == [1, 5, 2, 7]
assert donne_colonne(m, 2) == [4, 8, 3, 15]

# Autres tests
m2 = [[0, 1, 2, 3, 4, 5, 6],
    [7, 8, 9, 10, 11, 12, 13],
    [14, 15, 16, 17, 18, 19, 20]]

for j in range(7):
    assert donne_colonne(m2, j) == [m2[i][j] for i in range(3)]
    