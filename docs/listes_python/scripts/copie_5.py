liste_1 = [2, 11, 9, 10, 8, 19, 19, 4, 10, 12]
print("liste originale")
print("liste_1 : ", liste_1)

# Création d'un autre objet liste_2 par autre méthode
liste_2 = [note for note in liste_1]
print("liste_2 avant modification : ", liste_2)

# Modification de liste_2 : ajout de 1 point sur chaque note
for i in range(len(liste_2)):
    liste_2[i] = liste_2[i] + 1
print("liste_2 après ajout de 1 point : ", liste_2)

# Vérification de la liste de note originale
print("vérification de la liste originale : ", liste_1)