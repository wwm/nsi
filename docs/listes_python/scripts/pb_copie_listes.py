def remplacer_naif(valeurs, valeur_cible, nouvelle_valeur):
    valeurs_2 = valeurs
    for i in range(len(valeurs_2)):
        if valeurs_2[i] == valeur_cible:
            valeurs_2[i] = nouvelle_valeur
    return valeurs_2

    
valeurs = [3, 8, 7]
assert remplacer_naif(valeurs, 3, 0) == [0, 8, 7]
assert valeurs == [3, 8, 7]
