---
author: Mireille Coilhac
title: Critère de Chika
---


## Critère de divisibilité par 7

### 1.	Question 1

Regarder la vidéo suivante :

<iframe width="560" height="315" src="https://www.youtube.com/embed/VnUYtx_D_J4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

On rappelle qu’en Python `% a` donne le reste de la division par `a`.



Par exemple `17%3` renvoie 2 car $17 = 3 \times 5+2$

???+ question "Tester"

    {{IDE('scripts/modulo')}}


???+ question 

    A quoi peut servir cette opération ?

    ??? success "Solution"
   
        Cette opétation peut servir à récupérer les unités d'un nombre entier.
        

### 2. Question 2

 `nbre` est un entier composé de trois chiffres.

Ecrire une fonction `est_divisibe_par7_3ch` qui prend en paramètre un entier `nbre`, renvoie `True` si `nbre` est divisible par 7, et `False` sinon, **en utilisant le critère de Chika.**

Tester votre fonction en ajoutant des `assert` dans votre programme.

### 3. Question 3

En utilisant une liste en compréhension, écrire la liste nommée `liste_3`, de tous les entiers de trois chiffres multiples de 7.

### 4. Question 4

Ecrire un programme qui vérifie que pour tous les nombres `nbre` de `liste_3` la fonction `est_divisibe_par7_3ch` renvoie `True`.

### 5. Question 5

En utilisant des listes en compréhension, écrire la liste nommée `non_liste_3`, de tous les entiers de trois chiffres qui ne sont pas multiples de 7.

### 6. Question 6

Ecrire un programme qui vérifie que pour tous les nombres nbre de `non_liste_3` la fonction `est_divisibe_par7_3ch` renvoie `False`.

### 7. Question 7

`nbre` est un entier de quatre chiffres. Ecrire une fonction `est_divisibe_par7_4ch` qui prend en paramètre un entier `nbre`, renvoie `True` si `nbre` est divisible par 7, et `False` sinon, **en utilisant le critère de Chika.**  
Vous pourrez appeler la fonction du 2.

### 8. Question 8

En procédant comme auparavant, vérifier le critère de Chika pour tous les entiers de quatre chiffres. 
