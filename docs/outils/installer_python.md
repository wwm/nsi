---
author: Mireille Coilhac
title: Installer Python
--- 

## I. EduPython

* Nous allons travailler en classe avec EduPython 

* Si vous ne l’avez pas encore téléchargé chez vous :  
👉 Lien de téléchargement : [https://edupython.tuxfamily.org/](https://edupython.tuxfamily.org/){ .md-button target="_blank" rel="noopener" }

* En bas de la page cliquer sur « Télécharger la dernière version »

* Si vous avez déjà la version précédente (2.7), ce n’est pas la peine de télécharger celle-ci pour le moment.

## II. Thonny

Si vous ne parvenez pas à installer EduPython, vous pouvez installer Thonny.

👉 Lien de téléchargement : [https://thonny.org/](https://thonny.org/){ .md-button target="_blank" rel="noopener" }

Il y a moins de bibliothèques installées, mais certains outils sont très pratiques : 

* vérificateur de variables en haut à droite
* un débugueur
* un système d’évaluation des expressions étape par étape

## III. basthon

!!! info "basthon"

	😉 basthon signifie **ba**c à **s**able pour Py**thon** 3.

!!! info "Sur tablette"

	😊 basthon est un outil en ligne, vous pourrez donc sans aucune installation l'utiliser sur votre tablette.


👉 Suivre le lien :  [https://basthon.fr/](https://basthon.fr/){ .md-button target="_blank" rel="noopener" }

👉 Cliquer sur Console, puis sur Python

![basthon](images/basthon.png){ width=70% }

Vous obtenez à gauche l’éditeur et à droite la console.
En bas le menu vous permet d’exécuter, ouvrir, **télécharger** ce que vous avez fait (**il faudra absolument le faire pour sauvegarder votre travail dans votre Cloud**) etc.
Vous pouvez essayer ce qui est proposé dans cette barre d’outils, en particulier l'icône partager.

![console](images/console.png){ width=70% }