---
author: Jean-Louis Thirot et Mireille Coilhac
title: Python - Instructions conditionnelles
---

## I. La syntaxe par l'exemple


???+ question "Juste un if"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

	```python
	prix = 50
	if prix > 20:  # (1)
    	print("Ce prix dépasse 20 euros")
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    
    Tester :
    
    {{IDE('scripts/juste_if')}}

???+ question "if et else"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

	```python
	note = float(input("Saisir votre note : "))
	if note >= 10:  # (1)
    	print("reçu")
	else:  # (2)
    	print("refusé")
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.


    Tester :
    
    {{IDE('scripts/if_else')}}

???+ question "if, elif et else"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

	```python
	note = float(input("Saisir votre note : "))
	if note >= 16:  # (1)
    	print("TB")  # (2)
	elif note >= 14:  # (3)
    	print("B")  # (4)
	elif note >= 12:  # (5)
    	print("AB")  # (6)
	elif note >= 10:
    	print("reçu") # (7)
	else:  # (8)
    	print("refusé")  # (9)
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. Si on est arrivé là, cela signifie que `note` ≤ 16

    3. `elif` signifie **sinon si**.

    4. Si on est arrivé là, cela signifie que `note` < 16 et `note` ≥ 14 :   
    14 ≤ `note` < 16

    5. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    6. Si on est arrivé là, cela signifie que `note` < 14 et `note` ≥ 12 :   
    12 ≤ `note` < 14

    7. Si on est arrivé là, cela signifie que `note` < 12 et `note` ≥ 10 :   
    10 ≤ `note` < 12

    8. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    9. Si on est arrivé là, cela signifie que `note` < 10 . **On n'écrit jamais de condition après `else`**


    Tester :
    
    {{IDE('scripts/if_elif_else')}}

???+ question "if ... et if"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

	```python
	note = float(input("Saisir votre note : "))
	if note == 20:  # (1)
    	print("Parfait !")
	if note != 0:  # (2)
    	print("Ce n'est pas nul !")
    # (3)
    ```

    1. :warning: Il faut deux symboles `==` pour tester l'égalité. Un seul symbole `=`réalise une affectation.

    2. :warning: Différent s'écrit `!=`

    3. :warning: Il n'est pas obligatoire d'avoir un `else`


    Tester :

    Vous pourrez tester les saisies de 0, puis de 20, puis de 10 par exemple. Que se passe-t-il ?
    
    {{IDE('scripts/if_if')}}

    ??? success "Solution"

	    Si on saisit 0 ... il ne se passe rien ...
        
	    Lisez le code, c'est normal ! 😊
        
        Si on saisit 20, "on rentre" dans les deux tests `if`, et on obtient les deux affichages.


## II. Cours/TD if

### 1. Avec des triangles

???+ question "Le triangle est-il rectangle?"

    Voilà la question que l'on se pose : le triangle ci-dessous est-il rectangle ?

    ![triangle 3 4 5](images/triangle_3_4_5.png){ width=20% }

    Tester ci-dessous. Que se passe-t-il ?

    {{IDE('scripts/tri_3_4_5')}}

    ??? success "Solution"

        Bien sûr ce code affiche que ABC est triangle, puisque 25 est bien égal a 9 + 16. 

        $AC^2 == AB^2 + BC^2$ s'évalue à <font color=#66bb66><b>True</b></font>

        **Notez bien le `==` :  Ce n'est pas une faute de frappe, on a vraiment mis deux fois le signe =.  
        👉 En python :

        * a = 1 est une affectation (a prend la valeur 1)
        * a == 1 est une comparaison utilisant l'opérateur comparatif ==. Cet opérateur permet de tester une égalité : (ici la proposition est : a = 1 ? 
        * a == 1 peut prendre deux valeurs : Vrai ou Faux. En Python (comme dans la majorité des langages de programmation) c'est True ou False. C'est donc une proposition booléenne.

        * On dénomme très souvent les conditions ainsi : <b>expressions booléennes</b>.

        La structure de cette instruction conditionnelle est :

        ```python title=''
        if condition :
            Instruction(s)
        ```

???+ question "Et si le triangle n'était pas rectangle, qu'aurait fait notre programme ?"

    Essayons, en modifiant une longueur. Ci-dessous, on a mis `BC = 2`, et du coup, forcément, l'égalité de pythagore n'est plus vraie, et le triangle n'est pas rectangle.

    Tester ci-dessous. Que se passe-t-il ?

    {{IDE('scripts/tri_2_5_4')}}

    ??? success "Solution"

        Il ne se passe rien !

        L'instruction **print()** n'a pas été exécutée, parce que la condition n'est pas vérifiée. 

        Cette fois, l'expression booléenne $AC^2 == AB^2 + BC^2$ s'évalue à <font color=#66bb66><b>False</b></font>

???+ question "L'indentation"

    Afin de mieux visualiser le fonctionnement par bloc, nous allons ajouter quelques instructions dans notre code.

    Tester ci-dessous. Que se passe-t-il ?

    {{IDE('scripts/tri_3_4_5_long')}}

    ??? success "Solution"

        ![déroulé](images/deroule_if_.png){ width=90% }

        👉 On observe donc qu'il n'y a pas d'affichage dans le cas où le triangle n'est pas rectangle en B.

### 2. Pile ou face

!!! info "La fonction choice()"

    Dans cet exercice, nous allons utiliser  : **la fonction `choice()`**

    Cette fonction est disponible dans la **librairie** `random`.  

    Nous aurons l'occasion de reparler de tout ceci plus tard.  
    Pour le moment nous allons juste utiliser le petit code suivant.  
    **Vous devez l'exécuter plusieurs fois de suite.**

    {{IDE('scripts/choisir')}}


???+ question "Jeu de Pile ou Face"

    Le jeu est le suivant : 

    On simule avec le code précédant le lancer de pièce.  

    * Si on tombe sur `"pile"`  on gagne 1 euro. 
    * Si on tombe sur `"face"`  on perd 1 euro.  

    Compléter le code ci-dessous pour qu'il s'affiche "gagné 1 euro" ou "perdu 1 euro" en respectant la règle du jeu.

    {{IDE('scripts/pile_face')}}

    ??? success "Solution"

        ```python title=''
        from random import choice
        lancer = (choice(["pile", "face"]))
        print("obtenu : ", lancer)
        if lancer == "pile":
            print("gagné 1 euro")
        if lancer == "face":
            print("perdu 1 euro")
        ```

???+ question "Jeu de Pile ou Face avec deux lancers"

    Le jeu est maintenant le suivant   
    On simule avec le lancer deux pièces. 
    * Si les deux pièces tombent toutes les deux du même côté   on gagne 1 euro. 
    * Si une tombe sur `"pile"` et l'autre sur `"face"`  on perd 1 euro.  

    ??? tip "Le symbole différent"

        Rappel : en Python le symbole $\neq$ s'écrit `!=`

    Compléter le code ci-dessous pour qu'il s'affiche "gagné 1 euro" ou "perdu 1 euro" en respectant la règle du jeu.
 
    👉 Vous devrez l'exécuter plusieurs fois de suite pour bien observer les différentes possibilités.

    {{IDE('scripts/pile_face_2_essais')}}

    ??? success "Solution"

        ```python title=''
        from random import choice
        lancer_1 = (choice(["pile", "face"]))
        lancer_2 = (choice(["pile", "face"]))
        print("obtenu pour la pièce 1 : ", lancer_1)
        print("obtenu pour la pièce 2 : ", lancer_2)
        if lancer_1 == lancer_2:
            print("gagné 1 euro")
        if lancer_1 != lancer_2:
            print("perdu 1 euro")
        ```

## III. Cours/TD if ... else

!!! abstract "if...if"

    Dans les 2 exercices qui précèdent, vous pouvez remarquer qu'on fait à chaque fois un test puis son contraire :

    **exercice 1 :**

    * On teste d'abord si on a obtenu `"pile"`.
    * On teste ensuite le contraire, à savoir, si on a obtenu `"face"`.

    Pourtant, le premier test pourrait nous suffire : si oui, le second test echouera, et si non, le second test réussira.

    En clair, soit la pièce tombe sur "pile", soit elle ne tombe pas sur "pile" et on n'a pas besoin de se poser deux fois la même question !

    **exercice 2 :**  

    * soit les 2 pièces tombent du même côté
    * soit elles ne tombent pas du même côté.

    Là encore, il semble inutile de se poser deux fois la même question !

    En fait, dans ces deux exercices, on test une condition et on affiche une chose si elle est <font color=#66bb66><b>True</b></font> et une autre **sinon**.

!!! example "if ...else"

    👉Nous pouvons simplifier le code de l'exercice 1 ainsi :

    ```python title=''
    from random import choice
    lancer = (choice(["pile", "face"]))
    print("obtenu : ", lancer)
    if lancer == "pile":
        print("gagné 1 euro")
    else:
        print("perdu 1 euro")
    ```

    !!! info "else"

        `else` signifie **sinon**.

        Le code est donc très explicite :

        ```python title=''
        Si on a obtenu pile :
            afficher("gagné 1 euro")
        Sinon :
            afficher("perdu 1 euro")
        ```

???+ question "tester et comprendre"

    **1.** tester un lancer pile

    {{IDE('scripts/pile')}}

    **2.** tester un lancer face

    {{IDE('scripts/face')}}

    ??? success "Solution"

        Voià ce qu'il se passe : 

        ![pile face](images/pile_face.png){ width=90% }

???+ question "Jeu de Pile ou Face avec deux lancers et un else"

    Le jeu est maintenant le suivant   
    On simule avec le lancer deux pièces. 
    * Si les deux pièces tombent toutes les deux du même côté   on gagne 1 euro. 
    * Si une tombe sur `"pile"` et l'autre sur `"face"`  on perd 1 euro.  

    !!! warning "Contrainte"

        Utiliser obligatoirement `else`

    {{IDE('scripts/pile_face_2_essais_else')}}

    ??? success "Solution"

        ```python title=''
        from random import choice
        lancer_1 = (choice(["pile", "face"]))
        lancer_2 = (choice(["pile", "face"]))
        print("obtenu pour la pièce 1 : ", lancer_1)
        print("obtenu pour la pièce 2 : ", lancer_2)
        if lancer_1 == lancer_2:
            print("gagné 1 euro")
        else:
            print("perdu 1 euro")
        ```

## IV. Cours/TD if ... elif...else

La struction **if ... else ...** que nous venons de voir est souvent utilisée, quand on doit faire quelques chose si une condition est vraie, et autre chose sinon (si elle est fausse donc !).

Mais il y a aussi de nombreuses situations où on peut avoir plusieurs cas.

???+ question "Encore des triangles"

    Reprenons le triangle du début, et considérons maintenant 2 triangles :

    ![2 triangles](images/deux_triangles.png){ width=40% }

    Tester ci-dessous. Qu'en pensez-vous ?

    {{IDE('scripts/triangles_sans_elif')}}

    ??? success "Solution"

        Ce n'est pas satisfaisant !

        Pour le triangle de droite, le code a bien trouvé qu'il n'est pas rectangle en B. Ce qui est vrai. Mais il serait préférable 
        qu'il nous affiche que ce triangle est rectangle en C.

        En fait, il y a 3 égalités à vérifier pour savoir si le triangle est rectangle :

        * $AB^2 == AC^2 + BC^2$ 👉 le tirangle est rectangle en $C$
        * $AC^2 == AB^2 + BC^2$ 👉 le tirangle est rectangle en $B$
        * $BC^2 == AB^2 + AC^2$ 👉 le tirangle est rectangle en $A$

        Et si aucune des 3 conditions n'est vraie : le triangle n'est pas rectangle !

    
    Tester ci-dessous. Nous avons utilisé `elif`

    {{IDE('scripts/trois_triangles', MAX_SIZE=50)}}


!!! abstract "Résumé"

    Le code est donc très explicite :

    ```python title=''
    Si 1ere condition :
        bloc si cette condition est vraie
    Sinon si 2ème condition :
        bloc si cette condition est vraie
    Sinon si 3ème condition :
        bloc si cette condition est vraie
    Sinon :                          # <-- sinon ici signifie : si aucune des conditions qui précèdent n'est vraie
        bloc si toutes les conditions précédentes sont fausse
    ```

!!! info "A savoir"

    * Il peut y avoir autant de elif qu'on a de cas à tester
    * **Le `else` est facultatif** (parfois, on regarde plusieurs cas, mais si aucun n'est vrai, on ne fait rien et on n' a donc pas besoin de `else`)
    * **une seule condition sera exécutée** (celle qui est vraie ou celle du `else`, s'il y a un `else`)
    * Même si 2 conditions sont vraies ?   
    **oui !** voir les exemples ci dessous...


???+ question "Pile ou face avec conditions **non mutuellement exclusives**"

    Modifions le jeu : on lance 2 pièces, et si au moins une des deux est **pile** on a gagné, sinon on a perdu.

    Tester ci-dessous. Qu'en pensez-vous ?

    {{IDE('scripts/jeu_non_exclusif')}}

    ??? success "Solution"

        Nous sommes d'accord, les 2 conditions `lancer_1 == "pile"` et  `lancer_2 == "pile"`sont vraies toutes les deux ... 
        mais si on a gagné à la 1ere pièce, inutile de vérifier si on a encore gagné à la seconde !

        👉 Dans une structure if.. elif... else... on arrête les tests dès qu'une des conditions est vraie.

        * Dans l'exemple des triangles les conditions sont mutuellement exclusives :  
        Un triangle ne peut pas être rectangle et A et en même temps en B ou en C. Donc s'il est rectangle en A, inutile de regarder s'il
         est rectangle en B. Dès qu'un cas est vrai, on peut se passer de regarder les autres.

        * dans l'exemple des lancers de pièces  les conditions ne sont pas mutuellement exclusives :  
        En général, on utilise les structures elif avec des cas disjoint, c'est à dire pour des conditions mutuellement exclusives, ou 
        pour le dire plus simplement, quand il est impossible que 2 conditions soient vraies en même temps. Mais ce n'est pas obligé, l'exemple 
        des 2 pièces montre un tel exemple.


    
<!--

???+ question "Cours/TD"

    <div class="centre" markdown="span">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/instrucions_conditionnelles_2022_sujet.ipynb"
    width="900" height="700" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

    😊 Voici la correction : 

    <div class="centre" markdown="span">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/instructions_conditionnelles_2022_correction.ipynb"
    width="900" height="700" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
-->

<!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION

⏳ La correction viendra bientôt ... 
👉 Bien sortir ce commentaire de l'admonition en supprimant l'indentation
🌐 Fichier `instructions_conditionnelles_2022_correction.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/instructions_conditionnelles_2022_correction.ipynb)
-->

## V. Exercices

???+ question "Exercice 1 : la cible - version 1"

    Bob a trouvé un jeu de fléchette très simple : il lance une flechette sur le plateau et il gagne 1 point si la flechette atteint la croix rouge. 
    Le plateau est représenté ci-dessous :

    ![cible](images/croix_rouge.png){ width=20% }

    Bob a programmé le jeu sur son ordinateur. 

    Dans cet exercice, nous allons utiliser une nouvelle fonction : **la fonction `randint()`**  
    Cette fonction permet d'obtenir un nombre aléatoire. Par exemple, **randint(1, 6)** est un nombre aléatoire compris entre 1 et 6, inclus.

    Pour importer la fonction `randint()` de la librairie `random` (c'est-a-dire pouvoir l'utiliser dans notre code) nous ajoutons au début de notre code :  
    ```python title=''
    from random import randint
    ```

    **1.** Compléter le code ci-dessous :

    {{IDE('scripts/fleche_simple')}}

    ??? success "Solution"

        ```python title=''

        # On importe la fonction randint du module random
        from random import randint

        # Bob lance sa flechette, elle atteint une case au hasard du plateau :
        numero_ligne = randint(0, 4)
        numero_colonne = randint(0, 4)

        print("La flechette atteint la case située : Ligne :", numero_ligne,"et  Colonne :", numero_colonne)
        # a-t-il gagné ?
            
        if numero_ligne == 2 :
            print("Bob a gagné")
        elif numero_colonne == 2 :
            print("Bob a gagné")
        else :
            print("Bob à perdu")
        ```

    **2.** Si la condition `numero_ligne == 2` est vérifiée, est ce que la seconde condition, `numero_colonne == 2` peut aussi être vérifiée ?

    ??? success "Solution"

        Oui, s'il atteint la case (2, 2) les 2 conditions seront vérifiées. (ici les conditions ne sont pas mutuellement exclusives)

    **3.** Est-ce que le programme donne toujours le bon résultat ?

    ??? success "Solution"

        Le programme donne toujours le bon résultat :  
        
        * Lorsque la 1ère condition est vérifiée, il est inutile de vérifier la 2ème condition, car de toutes façons, il a gagné. 
        * Si la 1ère n'est pas vérifiée, mais la deuxième oui, il a aussi gagné.
        * Si aucune des deux n'est vérifiée, alors il a perdu.

    **4.** Alice propose un autre code : 

    ```python

    if numero_ligne == 2 :
        print("Bob a gagné")
    if numero_colonne == 2 :
        print("Bob a gagné")
    if numero_ligne != 2 and numero_colonne != 2 :
        print("Bob a perdu")
    ```
    Est-ce que le code d'Alice fonctionne dans tous les cas ? Comparez les avantages des deux codes....

    ??? success "Solution"

        Le code d'Alice fonctionne aussi bien que le code de Bob. Toutefois :

        * Il faudra toujours évaluer 3 expressions booléennes
        * Tandis que le code de Bob n'en évalue qu'une si on a joué la ligne  2, et deux sinon.

        En terme de lisibilité, le code d'Alice est plus difficile à lire.


???+ question "Exercice 2 : la cible - version 2"

    Bob modifié les règles du jeu. Voyez le nouveau plateau de jeu ci-dessous. La case rouge rapporte 100 points, les cases orange rapportent 50 points, 
    les cases blanches ne rapportent aucun point.

    ![cible jaune](images/croix_jaune.png){ width=20% }

    Complétez ci-dessous pour que le code affiche le nombre de points gagnés

    {{IDE('scripts/fleche')}}

    ??? success "Solution"

        ```python title=''
        from random import randint

        # Bob lance sa flechette, elle atteint une case au hasard du plateau :
        numero_ligne = randint(0, 4)
        numero_colonne = randint(0, 4)

        print("La flechette atteint la case située : Ligne :", numero_ligne,"et  Colonne :",numero_colonne)

        if numero_ligne == 2 and numero_colonne == 2:
            print("100 points")
        elif  numero_ligne == 2 or numero_colonne == 2:
            print("50 points")  
        else :
            print("0 points")
        ```
    
<!--
???+ question "Exercice"

	👉 Il faut d'abord télécharger les trois fichiers suivants et les mettre dans le même dossier :

	🌐 TD à télécharger : Fichier `exos_instructions_conditionnelles_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/exos_instructions_conditionnelles_sujet.ipynb)

	🌐 module à télécharger : Fichier `_module_ex_conditionelles.py` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/_module_ex_conditionelles.py)

	🌐 image à télécharger : Fichier `plateau.png` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/plateau.png)

	👉 Il faut ensuite ouvrir le premier fichier exos_instructions_conditionnelles_sujet.ipynb en ligne avec [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    😊 La correction est arrivée : 

    🌐 Fichier `exos_instructions_conditionnelles_correction.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/exos_instructions_conditionnelles_correction.ipynb)
-->


<!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION
⏳ La correction viendra bientôt ... 
👉 Bien sortir ce commentaire de l'admonition en supprimant l'indentation
🌐 Fichier `exos_instructions_conditionnelles_correction.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/exos_instructions_conditionnelles_correction.ipynb)
-->

## VI. Bilan

!!! danger "Attention à ne pas oublier les deux points et l'indentation."


!!! abstract "La syntaxe de if :"

    👉 Attention, l'instructions `else` n'est pas du tout obligatoire (Voir les exemples ci-dessus).  
	
    * Avec seulement `if`
	```pyton title=''
	if condition :
   		bloc d'instructions à réaliser
   	...
   	```

    * Avec `if` et `else`
   	```pyton title=''
	if condition :
   		bloc d'instructions à réaliser
   	else:
   		autre bloc d'instructions à réaliser
   	...
   	```


!!! abstract "La syntaxe de if ... elif ... else :"

    👉 Attention, l'instructions `else` n'est pas du tout obligatoire (Voir les exemples ci-dessus).  

    * Avec seulement `if` et `elif`	
	```title=''
	if condition :
   		bloc d'instructions à réaliser
   	elif autre condition:
   		autre bloc d'instructions à réaliser
   	...
   	```

    * Avec `if` ... `elif` ... `else`
	```title=''
	if condition :
   		bloc d'instructions à réaliser
   	elif autre condition:
   		autre bloc d'instructions à réaliser
   	else:
        encore un autre bloc d'instructions à réaliser
   	...
   	```

## VII. Autres exercices

???+ question "Exercice 1 **papier**"

	```python linenums='1'
	a = 10
	b = 13
	if a > 5:
	    b = b - 4
	if b >= 11:
	    b = b + 10
	```

    Que vaut la valeur finale de b ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 13
        - [ ] 9
        - [ ] 19
        - [ ] 23

    === "Solution"

        - :x: ~~13~~ Faux car 10 > 5. La ligne 4 et donc exécutée.
        - :white_check_mark: 9 
        - :x: ~~19~~ 
        - :x: ~~23~~ Faux car à la ligne 4 on a une nouvelle affectation de b qui ne respecte plus la condition du if de la ligne 5.


???+ question "Exercice 2 'papier'"

	```python linenums='1'
	a = 5
	b = 14
	if a > 9:
	    b = b - 1
	if b >= 10:
	    b = b + 8
	```

    Que vaut la valeur finale de b ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 22
        - [ ] 21
        - [ ] 13
        - [ ] 14

    === "Solution"

        - :white_check_mark: 22 
        - :x: ~~21~~ Faux car la ligne 4 n'est pas exécutée
        - :x: ~~13~~ Faux car la ligne 4 n'est pas exécutée
        - :x: ~~14~~ Faux car à la ligne 6 est exécutée


???+ question "Exercice 3 'papier'"

	```python linenums='1'
	a = 7
	b = 20
	if a > 5:
	    b = b - 3
	if b >= 15:
	    b = b + 7
	```

    Que vaut la valeur finale de b ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 24
        - [ ] 17
        - [ ] 20
        - [ ] 27

    === "Solution"

        - :white_check_mark: 24 
        - :x: ~~17~~ Faux car la ligne 6 est aussi exécutée
        - :x: ~~20~~ Faux car la ligne 4 et la ligne 6 sont exécutées.
        - :x: ~~27~~ Faux car à la ligne 4 est exécutée


???+ question "Exercice 4 'papier'"

	```python linenums='1'
	a = 2
	b = 14
	if a >= 0:
	    b = b - 2
	else:
		b = b + 2	
	if b > 0:
	    a = a + 1
	else:
		a = a - 1
	```

    Que vaut la valeur finale de **`a`** ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 3
        - [ ] 16
        - [ ] 1
        - [ ] 12

    === "Solution"

        - :white_check_mark: 3 
        - :x: ~~16~~ Faux car la ligne 6 n'est pas exécutée.
        - :x: ~~1~~ Faux car la ligne 10 n'est pas exécutée.
        - :x: ~~12~~ Faux car on cherche la valeur finale de a.

???+ question "Exercice 5 'papier'"

	```python linenums='1'
	a = -3
	b = -11
	if a >= 0:
	    b = b - 1
	else:
		b = b + 1	
	if b > 0:
	    a = a + 3
	else:
		a = a - 3
	```

    Que vaut la valeur finale de **`a`** ?

    === "Cocher la ou les affirmations correctes"

        - [ ] -10
        - [ ] -6
        - [ ] 0
        - [ ] -12

    === "Solution"

        - :x: ~~-10~~ La ligne 6 est exécutée, mais on demande a.
        - :white_check_mark: -6. La ligne 6 et la ligne 10 sont exécutée. On demande a.
        - :x: ~~0~~ Faux car la ligne 8 n'est pas exécutée.
        - :x: ~~-12~~ 

???+ question "Exercice 6 'papier'"

    ```python linenums='1'
    a = -7
    b = 14
    if a >= 0:
        b = b - 4
    else:
        b = b + 4   
    if b > 0:
        a = a + 1
    else:
        a = a - 1
    ```

    Que vaut la valeur finale de **`a`** ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 10
        - [ ] -8
        - [ ] 18
        - [ ] -6

    === "Solution"

        - :x: ~~-10~~
        - :x: ~~-8~~. La ligne 10 n'est pas exécutée.
        - :x: ~~18~~ Faux car la ligne 6 est exécutée mais on demande a.
        - :white_check_mark: -6. La ligne 6 et la ligne 8 sont exécutées. On demande a.

???+ question "Exercice 7 'papier'"

    ```python linenums='1'
    a = 10
    b = -11
    if a >= 0:
        b = b - 3
    else:
        b = b + 3   
    if b > 0:
        a = a + 3
    else:
        a = a - 3
    ```

    Que vaut la valeur finale de **`a`** ?

    === "Cocher la ou les affirmations correctes"

        - [ ] -8
        - [ ] 7
        - [ ] -14
        - [ ] 13

    === "Solution"

        - :x: ~~-8~~ 
        - :white_check_mark: 7 . Les lignes 4 et 10 sont exécutées.
        - :x: ~~-14~~ Faux car la ligne 4 est exécutée mais on demande a.
        - :x: ~~13~~ Faux car la ligne 8 n'est pas exécutée. 


???+ question "Exercice 8 'papier'"

    ```python linenums='1'
    a = -20
    b = -7
    c = -2
    if a > b:
        d = a
    elif b > c:
        d = b
    else:
        d = c
    ```

    Que vaut la valeur finale de **`d`** ?

    ??? success "Solution"

        -2

???+ question "Exercice 9 'papier'"

    ```python linenums='1'
    a = 10
    b = 19
    c = -15
    if a > b:
        d = a
    elif b > c:
        d = b
    else:
        d = c
    ```

    Que vaut la valeur finale de **`d`** ?

    ??? success "Solution"

        19


