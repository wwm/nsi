---
author: Mireille Coilhac
title: Fonctions - Généralités
---

## I. Les fonctions en Python

### Notion de fonction

!!! info "Les fonctions"

	En informatique, les fonctions servent à mieux structurer votre code. On dit aussi que les fonctions servent à **factoriser** le code. Elles permettent d'éviter de répéter plusieurs fois des portions de codes identiques. Ainsi, une fonction peut être vue comme un «petit» programme :

	* à qui on donne des paramètres en entrée,
	* puis qui effectue alors un traitement sur ces paramètres, dans le corps de la fonction.
	* qui **renvoie** enfin un résultat en sortie.

	![fonction](images/fig1.png){ width=35% }

	Une fonction qui modifie des variables mais sans renvoyer de résultat est appellée une **procédure**. Le langage Python ne fait pas de différence dans la syntaxe entre fonction et procédure.

### Syntaxe en Python

En Python, une fonction peut s'écrire en suivant toujours le même formalisme :

!!! info "Syntaxe"

	* Le mot clé **`def`** +  nom de la fonction + les paramètres entre parenthèses (pour chacun un nom de variable). + Terminer par deux points **`:`**
	* Si la fonction ne prend aucun paramètres, on met quand même les parenthèses sans rien à l'intérieur.
	* En dessous, écrire le blocs des instructions. Attention il faut **indenter** (décaler) ce bloc.
	* Si la fonction renvoie un résultat, finir en dernière ligne par le mot clé **`return`** suivi de ce que renvoie la fonction. Attention, cette ligne est indentée également et marque la fin de la fonction.

	Visuellement cela donne quelque chose comme ceci: 

	```python
	def ma_fonction(liste de parametres):
		instructions
		return resultat
	```


!!! danger "return"

	Dès que l'instruction `return` est exécutée, on "sort" de la fonction.

	Un `return` dans une boucle provoque donc une sortie anticipée de la boucle.

	🌵 Il ne faut pas l'oublier ...
	


### Appeler (utiliser) une fonction En Python

!!! info "Appel de fonction"

    Lorsque l'on définit une fonction, on ajoute une fonctionalité à python, mais la fonction n'est pas exécutée. Elle ne le sera que lorsque l'on va appeler cette fonction.


    Une fonction est utilisée comme une instruction quelconque. Un appel de fonction est constitué du nom de la fonction suivi entre parenthèses des valeurs des paramètres d'entrée. Cet appel peut être fait :

    * Soit, comme nous l'avons vu, dans le programme principal (ou dans une autre fonction)

    ```python title=''
    def double(nombre) :
        return 2*nombre

    a = 5
    reponse = double(a) # La fonction est appelée ici
    print("le double de ", a,"vaut :", reponse)
    ```

    * Soit par un appel en console : 

    ```python title=''
    >>>double(6)
    12
    ```

### Vocabulaire :

!!! info "paramètre et argument"

	Lorsqu'on écrit `double(nombre)`, `nombre` est appelé **paramètre** de la fonction `double`.

	Lorsqu'on appelle la fonction `double` avec une valeur explicite pour `nombre` comme dans `double(6)`, on dit que `6` est un **argument** de la fonction `double`. 

	Ainsi, si on **appelle** la fonction `double`avec l'argument 5, celle-ci **renvoie** `10` .


## II. Portée des variables

Suivre ce lien, et bien l'étudier :

 [Portée des variables](http://lptms.u-psud.fr/wiki-cours/index.php/Python:_Port%C3%A9e_des_variables){ .md-button target="_blank" rel="noopener" }


## III. Assertions


???+ question "Tester - 1"

    Exécuter le script ci-dessous :

    {{IDE('scripts/carre')}}


    ??? success "Que s'est-il passé ?"

        Il ne se passe rien, le `assert` a bien été vérifié.



???+ question "Tester - 2"

    Exécuter le script ci-dessous :

    {{IDE('scripts/puissance')}}


    ??? success "Que s'est-il passé ?"

        😢 On a une erreur : assertion erreur.  

        Le message de l'erreur est celui que nous avons indiqué sur la ligne `assert`



!!! info "`assert`"

    👉`assert` est souvent utilisé pour signaler qu’il y a une erreur dans le code de la fonction.   

    👉 Lorsque l'on crée une fonction, on crée en même temps un **"jeu de tests"** que l'on vérifiera avec des `assert`


???+ question "Tester - 3"

    Corrigeons notre fonction. Tester ci-dessous :

    {{IDE('scripts/puissance_juste')}}


    ??? success "Que s'est-il passé ?"

        On constate que tous les tests se sont bien passés. 


!!! abstract "🐘 A retenir"

    A partie de maintenant, vous écrirez toujours les assert avant de réaliser le code de votre fonction.    

    😊 C'est ainsi que l'on doit mettre au point un programme


!!! warning "Remarque"

    🌵 Souvent, on n'écrit pas de message dans l'assertion.  

    Voir ci-dessous.


???+ question "À vous de jouer"

    Compléter la fonction suivante qui calcule la somme $1+2+...+n$  

    Ajouter un jeu de tests avec des `assert` (Les messages ne sont pas obligatoires)

    {{IDE('scripts/somme')}}

    ??? success "Solution"

        ```python
        def somme(n):
            resultat = 0
            for i in range(n + 1):
                resultat = resultat + i
            return resultat

        assert somme(0) == 0
        assert somme(3) == 6
        assert somme(4) == 10
        ```

## IV. Exercices

???+ question "Exercice 1 : promotions"

    🎁 Dans une parfumerie, on propose deux promotions différentes pour l'achat de deux articles :  

    * Formule A : une réduction de 50% sur le prix du 2ème article (le moins cher)  
    * Formule B : une réduction de 20% sur le montant total à payer.  

    Muni d'un billet de 50€, Nicolas souhaite offrir deux parfums à sa mère . 

    **1.**	Ecrire une fonction `promo_A` qui prend en paramètres `prix1` et `prix2` et qui renvoie le prix total à payer pour l’achat de deux parfums aux prix `prix1` et `prix2`.  
    Par exemple : 	
    
    * `promo_A(10, 20)` doit renvoyer `25`, et
    * `promo_A(20, 10)` doit également renvoyer `25`.

    Compléter ci-dessous. Il faut exécuter votre code, et ensuite **obligatoirement le valider** (<span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="../../assets/images/icons8-check-64.png" alt="Valider"></button><span>) pour voir la solution.

    {{IDE('scripts/promo_a')}}   

    **2.** Ecrire de même,  une fonction `promo_B`.   
    Par exemple : 	
    
    * `promoB(10, 20)` doit renvoyer 24
    * `promoB(20, 10)` doit également renvoyer 24.

    Compléter ci-dessous.

    {{IDE('scripts/promo_b')}}

    **3.** Ajouter le programme principal :  
    Il doit demander la saisie du prix des deux parfums, afficher le prix avec la formule A et la formule B, Afficher quelle est la formule la plus avantageuse, et si Nicolas pourra offrir les deux parfums à sa mère (il ne dispose que de 50 euros).  
    Vous testerez votre code pour :  

    * Un parfum à 10 € et un autre à 20 €  
    * Un parfum à 12 € et un autre à 45 €  
    * Un parfum à 35 € et un autre à 20 €  

    Les fonctions `promo_A` et `promo_B` sont dans du code caché. Il est inutile de les écrire.

    {{IDE('scripts/promos')}} 

    ??? success "Solution"

        ```python title=''
        prix_1 = float(input("prix_1 = "))
        prix_2 = float(input("prix_2 = "))
        prix_final_A = promo_A(prix_1, prix_2)
        prix_final_B = promo_B(prix_1, prix_2)
        print("le prix final a payer avec la promotion A est de : ", prix_final_A, "euros")
        print("le prix final a payer avec la promotion B est de : ", prix_final_B, "euros")

        if prix_final_A < prix_final_B:
            print("la promotion A est plus avantageuse que que la promotion B")
            prix_final = prix_final_A

        elif prix_final_A == prix_final_B:
            print("les deux promotions donnent des prix identiques")
            prix_final = prix_final_A

        else:
            print("la promotion B est plus avantageuse que que la promotion A")
            prix_final = prix_final_B

        if prix_final > 50:
            print("Nicolas ne peut pas acheter, il n a que 50 euros")   
        ```

???+ question "Utilisation de la tortue"


    {{ IDE('scripts/essai_tortue') }}


    <div id="cible_1" class="admonition center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
    Le  tracé sera affiché ici
    </div>

???+ question "Exercice 2 : Des triangles avec la tortue"

    **1.** Tester ci-dessous

    {{ IDE('scripts/triangle') }}

    <div id="cible_2" class="admonition center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
    Le  tracé sera affiché ici
    </div>

    **2.** Compléter ci-dessous pour qu'il s'affiche :

    ![3 triangles](images/triangles_tortue.png){ width=30% }

    !!! warning "Contrainte"

        Il est **interdit** de modifier la fonction.  
        Il faudra l'appeler plusieurs fois.


    {{ IDE('scripts/turtle_3_triangles') }}

    <div id="cible_3" class="admonition center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
    Le  tracé sera affiché ici
    </div>

    ??? success "Solution"

        ```pyhon title=''
        from turtle import *
        def triangle(n): # n est le nombre de pixels
            for i in range(3):
                forward(n)
                left(120)

        n = int(input("nombre de pixels : "))
        left(30)
        triangle(n)
        for i in range(2):
            left(120)
            triangle(n)        
        ```

    **3.** Question facultative : Compléter ci-dessous pour qu'il s'affiche :

    ![3 triangles autres](images/triangles_tortue_2.png){ width=30% }

    !!! warning "Contrainte"

        Il est **interdit** de modifier la fonction.  
        Il faudra l'appeler plusieurs fois.


    {{ IDE('scripts/turtle_3_triangles_2') }}

    <div id="cible_4" class="admonition center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
    Le  tracé sera affiché ici
    </div>

    ??? success "Solution"

        * Si votre figure correspond à ce que vous avez demandé, c'est que c'est juste. 👍
        * Sinon, réfléchissez encore ! 😅 Cette question est facultative ...

        ??? success "Solution"

            ```pyhon title=''
            from turtle import *
            def triangle(n):
                for i in range(3):
                    forward(n)
                    left(120)


            n=int(input("nombre de pixels : "))
            left(30)
            triangle(n)
            forward(n)
            triangle(n)
            left(120)
            forward(n)
            right(120)
            triangle(n)
            ```

???+ question "Exercice 3 : Des maximums"

    **1.** Ecrire une fonction `maxi_2` qui prend en paramètre deux nombres entiers `n1` et `n2` et renvoie le plus grand des deux.

    {{ IDE('scripts/maximum_2') }}

    **2.** Ecrire une fonction `maxi_3` qui prend en paramètre trois nombres entiers `n1`, `n2` et `n3` et renvoie le plus grand des trois.

    !!! warning "Contrainte"

        Vous appelerez **obligatoirement** la fonction `maxi_2`

    {{ IDE('scripts/maximum_3') }}


???+ question "Exercice 4 : Des voyelles"

    **1.** Recopier, une à une les instructions ci-dessous dans la console, et observer le résultat

    ```pycon title=''
    >>> "e" in "aeiouy"
    ```

    ```pycon title=''
    >>> "f" in "aeiouy" 
    ```

    {{ terminal() }}

    ??? success "Solution"

        ```pycon title=''
        >>> "e" in "aeiouy"
        True
        >>> "f" in "aeiouy" 
        False
        ```

    Tester ci-dessous :

    {{ IDE('scripts/hello') }}

    **2.** Compléter la fonction suivante qui renvoie le nombre de voyelles dans `mot`.  
    Vous pourrez utiliser les syntaxes vues au **1.**

    {{ IDE('scripts/nb_voyelles') }}
     

???+ question "Exercice 5 : La suite de Syracuse"

    La conjecture de Syracuse (ou de Collatz) postule ceci :

    Prenons un nombre $n$ :

    * si $n$ est pair, on le divise par 2
    * sinon on le multiplie par 3 puis on ajoute 1. 

    On recommence cette opération tant que possible. Au bout d'un certain temps, on finira toujours par tomber sur le nombre 1.

    **1.** Compléter la fonction `suivant` qui prend en paramètre un entier `n` et renvoie son successeur en respectant les règles énoncées ci-dessus.

    {{ IDE('scripts/suivant_syracuse') }}

    **2.** Écrire une fonction `syracuse` qui prend en paramètre un entier `n` et qui affiche tous les termes de la suite de Syracuse jusqu'à 1 (on l'espère !).  
    Vous appelerez obligatoirement la fonction du **1.**

    {{ IDE('scripts/termes_syracuse') }}

    ??? success "Solution"

        ```python title=''
        def syracuse(n):
            print(n)
            while n != 1:
                n = suivant(n)
                print(n)
        ```

    **3.** Écrire une fonction `temps_de_vol` qui prend en paramètre un entier `n` et qui renvoie le nombre d'étapes pour arriver à 1, en partant de `n`.   
    Par exemple `temps_de_vol(5)` doit renvoyer 5.

    {{ IDE('scripts/temps_vol') }}

    **4.** Compléter la fonction suivante pour afficher le nombre de départ inférieur ou égal à `n` donnant le plus grand temps de vol, ainsi que le temps de vol correspondant . Vous pourrez uppeler les fonctions écrites aux questions précédentes.

    {{ IDE('scripts/temps_maxi') }}

    
<!--- old

???+ question "Exercices - 1"

    Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    🌐 TD à télécharger : Fichier `fonctions_serie1_2022_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/fonctions_serie1_2022_sujet.ipynb)

    ⏳ La correction viendra bientôt ... 

-->
<!--- old

<div class="centre">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/fonctions_serie1_2022_sujet.ipynb"
width="900" height="900" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

😊 La correction est arrivée 

<div class="centre">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/corr_fonctions_serie1_2022.ipynb"
width="900" height="900" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>
-->

<!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION
👉 Bien sortir ce commentaire de l'admonition en supprimant l'indentation
??? success "Solution"

    	🌐 Correction à télécharger : Fichier `corr_fonctions_serie1_2022.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/corr_fonctions_serie1_2022.ipynb)
-->




<!--- old

<div class="centre">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/fonctions_serie_2_2023_sujet.ipynb"
width="900" height="900" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


😊 La correction est arrivée 

<div class="centre">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/fonctions_serie_2_2023_corr.ipynb"
width="900" height="900" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>
-->

<!---
⏳ La correction viendra bientôt ... 
-->



## V. Bilan

!!! example "Exemple"

    ```python
    def prix(nbre_adultes, nbre_enfants):                   # définition
        resultat = 37 * nbre_adultes + 28 * nbre_enfants    |
        return resultat                                     | corps de la fonction (bloc indenté)
        
    prix(3, 2)                                              # Appel de la fonction prix
    ```     

!!! info "Définition d'une fonction"    

    La 1ère ligne de la fonction est la **définition** de la fonction :
    ```python
    def prix(nbre_adultes, nbre_enfants):
    ```    
    Elle commence par le mot clef <span style="color:blue;">def</span>, suivie du **nom** de la fonction, puis entre parenthèse, les **paramètres** de la fonction.

    Elle se termine par <span style="color:red;">:</span> qui indique qu'en dessous va se trouver un bloc d'instructions.
    

!!! info "Paramètres d'une fonction"

    Dans `def prix(nbre_adultes, nbre_enfants)`, on dit que `nbre_adultes` et `nbre_enfants` sont des **paramètres** de la fonction. 

    Ce sont des variables dont les valeurs seront déterminées lors de **l'appel de la fonction**.

    ```python
    def prix(nbre_adultes, nbre_enfants):                   
        resultat = 37 * nbre_adultes + 28 * nbre_enfants  # (1)
        return resultat                                    
    ``` 
        
    1. 👉 **Remarque importante :** Cette ligne utilise les variables `nbre_adultes` et `nbre_enfants` qui ne sont pas initialisée dans la fonction. C'est normal, comme nous l'avons dit juste au-dessus, **ces variables seront initialisées lors de l'appel de la fonction.**

    !!! warning "Prenez le temps de lire les commentaires (cliquez sur le +)"

!!! info "Corps de la fonction"

    ```python
    def prix(nbre_adultes, nbre_enfants):                   
        resultat = 37 * nbre_adultes + 28 * nbre_enfants 
        return resultat  # (1)                               
    ``` 
       
    1. `return ` signifie **renvoyer**, c'est cette ligne qui indique **ce que va renvoyer la fonction**. 
        Ici elle renvoie resultat, donc elle renvoie **la valeur de la variable resultat**

    !!! warning "Prenez le temps de lire les commentaires (cliquez sur le +)"

!!! info "**Appel** de la fonction"

    ```python
    prix(3, 2)
    ```

    Cette ligne de code ne fait pas partie de la fonction (Elle n'est pas dans le bloc indenté). Les lignes de codes qui ne sont pas dans une fonction font partie de ce qu'on appelle le **programme principal**.

    Une fois qu'une fonction est définie, il est possible de l'appeler (l'utiliser) dans le programme principal, mais aussi dans une autre fonction.

!!! info "Utiliser le résultat renvoyé par une fonction"
       
    Toutefois, l'appel ci-dessus ne fait qu'appeler la fonction, qui **renvoie** le résultat. Ceci ne sera pas utile si nous ne conservons pas ce résultat (afin de pouvoir l'utiliser dans la suite du programme). Pour cela nous allons en général **affecter** ce résultat à une variable :
        
    ```python
    prix_a_payer = prix(3, 2)
    print("le prix à payer : ", prix_a_payer) 
    ```

    Si on veut seulement afficher le résultat, on peut directement afficher ainsi :

    ```python
    print("le prix à payer : ", prix(3, 2)) 
    ```

!!! info "Arguments de la fonction" 
    
    Quand on **« appelle »** `prix(3, 2)` :   
    •	3 est automatiquement affecté au 1er paramètre : la variable  `nbre_adultes`  
    •	2 est automatiquement affecté au second paramètre : la variable  `nbre_enfants`

    3 et 2 sont les valeurs données en arguments.
    
### Remarque 1 : 

???+ note dépliée "Une fonction sans paramètres"

    Certaines fonctions n'ont aucun paramètre. Dans ce cas, on met des parenthèses vides aussi bien dans la définition que dans l'appel de la fonction :
    
    ```python
    def ma_fonction() :
        instruction du bloc
    
    ma_variable  = ma_fonction()
    ```

    
### Remarque 2 : 

???+ note dépliée "Des fonctions qui ne renvoient rien"

    Certaines fonctions ne renvoient rien.  
    
    Exemple :
    
    ```python
    def ma_fonction(nom) :
        print("Votre nom est :",nom)
        return None # ou simplement return, ou pas de return du tout ...
    
    ma_fonction()
    ```
    Cette fonction **ne renvoie rien** 
        
    De telles fonctions sont, dans certains langages, appelées des procédures. En python, on ne fait pas de différence. Une procédure **fait quelques chose** : ici par exemple, elle sert à afficher (dans la console) un message.
        
    Notez que dans l'appel d'une procédure, on n'affecte pas le résultat à une variable. C'est logique car il n'y a pas de résultat, puisque la fonction ne renvoie rien.

### Notion d'espace de noms
    
!!! info "Portée des variables"

    Une variable définie dans une fonction n'est connue qu'à l'"intérieur" de celle-ci.

    Par exemple si on exécute ceci: 

    ```python
    def prix(nbre_adultes, nbre_enfants):                  
        resultat = 37 * nbre_adultes + 28 * nbre_enfants    
        return resultat                                    
        
    prix(resultat)      # Cette ligne provoquera un message d'erreur !
    ```    

    On obtient le message : 
    ```python
    NameError: name 'resultat' is not defined
    ```

    En effet, la variable `resultat` n'est connue qu'à l'"intérieur" de la fonction


!!! note "Définitions :heart:"

    - Les variables définies dans le corps d'une fonction sont appelées **variables locales**.
    - Les variables définies dans le corps du programme (sous-entendu : pas à l'intérieur d'une fonction) sont appelées **variables globales**.


!!! note "Règles d'accès aux variables locales et globales :heart:"

    - **règle 1 :** une **variable locale** (définie au cœur d'une fonction) est **inaccessible** hors de cette fonction.
    - **règle 2 :** une **variable globale** (définie à l'extérieur d'une fonction) est **accessible** en **lecture** à l'intérieur d'une fonction.
    - **règle 3 :** une **variable globale** (définie à l'extérieur d'une fonction) **ne peut pas être modifiée** à l'intérieur d'une fonction.


![global_regles.png](images/global_regles.png){ width=80%; : .center }
> Source : Gilles Lassus

???+ question "Tester les variables locales et globales - 1"

    Exécuter le code ci-dessous, et comprendre le message d'erreur :

    {{IDE('scripts/local_global_1')}}

???+ question "Tester les variables locales et globales - 2"

    Exécuter le code ci-dessous, et comprendre ce qui se passe :

    {{IDE('scripts/local_global_2')}}

!!! info "Testez vos fonctions"
    
    * Choisir un cas ou vous pouvez, sans l'aide du code, déterminer ce que la fonction doit renvoyer.   
    
    * Ajouter l'instruction assert 
        
    Exemple d'utilisation pour une fonction `carre` qui élève au carré: 
        
    ```python
        assert carre(3) == 9, "l'appel carre(3) devrait renvoyer 9"
    ```    

    Si le test est réussi, il ne se passera rien, sinon le code **lève une exception AssertionError** et affiche le message.
        
    👉 Remarque : souvent nous n'écrirons pas de message explicatif. Nous nous contenterons par exemple de : 
        
    ```python
        assert carre(3) == 9
    ```  
        
    Dans ce cas-là, si le test est réussi, il ne se passera rien, sinon le code **lève une exception AssertionError** et n'affiche pas de message.

!!! info "Une fonction peut appeler une autre fonction"

    Toutes nos fonctions (que nous écrivons ou que nous importons) sont définies avant toute exécution du programme. De sorte que, lorsque l'exécution du code commence, elles sont toutes reconnues et utilisables en tout point du code, y compris dans d'autres fonctions. 
    



