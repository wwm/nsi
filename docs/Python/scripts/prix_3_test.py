# Tests
assert prix_etudiants(1, 3, 2) == 181

# Autres tests
assert prix_etudiants(13, 15, 9) == 1171
assert prix_etudiants(1258, 1541, 219) == 96264
assert prix_etudiants(0, 0, 0) == 0