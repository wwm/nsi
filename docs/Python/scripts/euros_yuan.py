def euro_vers_dollar(euros):
    return euros * 1.19 # en supposant qu'un euro vaut 1,19 dollars

def dollar_vers_yuan(dollars):
    return dollars * 6.93 # en supposant qu'un dollar vaut 6,93 yuans

def euro_vers_yuan(montant):
    # Appel de la fonction euro_vers_dollar
    montant_dollar = euro_vers_dollar(montant)
    # Appel de la fonction dollar_vers_yuan
    montant_yuan = dollar_vers_yuan(montant_dollar)
    # Valeur renvoyée
    return montant_yuan

euros = float(input("entrez le montant en euros :"))
montant_converti = euro_vers_yuan(euros)
print(euros, "€ représente ", montant_converti, "yuan")



