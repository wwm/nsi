nb_de_notes = 4

somme = 0  # Initiaisation de l'accumulateur
compteur = ...  # initialisation du compteur
    
for i in range(nb_de_notes):
    note = int(input("Entrez la note numéro " + str(i + 1) + ":"))
        
    # on ajoute cette note dans le total seulement si elle est paire :
    if note % 2 == 0 :
        somme = somme + note
        compteur = ...

# calcul de la moyenne :
moyenne = ...

print("La moyenne est : ", moyenne)
