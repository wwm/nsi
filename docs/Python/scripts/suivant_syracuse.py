# --- PYODIDE:code --- #

def suivant(n):
    ...


# --- PYODIDE:corr --- #

def suivant(n):
    if n % 2 == 0:
        return n // 2
    else:
        return 3*n + 1


# --- PYODIDE:tests --- #

assert suivant(5) == 16
assert suivant(16) == 8
assert suivant(8) == 4
assert suivant(4) == 2
assert suivant(2) == 1
assert suivant(1) == 4
assert suivant(4) == 2

# --- PYODIDE:secrets --- #

assert suivant(6) == 3
assert suivant(11) == 34