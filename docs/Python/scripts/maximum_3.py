# --- PYODIDE:env --- #

def maxi_2(n1, n2):
    if n1 < n2 :
        return n2
    else :
        return n1

# --- PYODIDE:code --- #

def maxi_3(n1, n2, n3):
    ...


# --- PYODIDE:corr --- #

def maxi_3(n1, n2, n3):
    temp = mmaxi_3(n1, n2)
    resultat = maxi_3(p, n3)
    return resultat

# --- PYODIDE:tests --- #

assert maxi_3(5, 5, 0) == 5
assert maxi_3(7, 7, 7) == 7
assert maxi_3(8, 5, 2) == 8
assert maxi_3(8, 5, 10) == 10


# --- PYODIDE:secrets --- #

assert maxi_3(5, 6, 5) == 6
assert maxi_3(5, 6, 4) == 6
