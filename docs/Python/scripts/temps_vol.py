# --- PYODIDE:env --- #

def suivant(n):
    if n % 2 == 0:
        return n // 2
    else:
        return 3*n + 1


# --- PYODIDE:code --- #

def temps_de_vol(n):
    ...


# --- PYODIDE:corr --- #

def temps_de_vol(n):
    compteur = 0
    while n != 1:
        compteur = compteur + 1
        n = suivant(n)
    return compteur


# --- PYODIDE:tests --- #

assert temps_de_vol(5) == 5
assert temps_de_vol(53) == 11

# --- PYODIDE:secrets --- #

assert temps_de_vol(97) == 118
