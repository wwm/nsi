---
author: Mireille Coilhac
title: Fonctions - découverte
---

## I. Première situation : le parc d'attraction

<center>
<a title="user:Solipsist, CC BY-SA 2.0 &lt;https://creativecommons.org/licenses/by-sa/2.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Chair-O-Planes,_night.jpg"><img width="256" alt="Chair-O-Planes, night" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Chair-O-Planes%2C_night.jpg/512px-Chair-O-Planes%2C_night.jpg"></a>

user:Solipsist, CC BY-SA 2.0 <https://creativecommons.org/licenses/by-sa/2.0>, via Wikimedia Commons
</center>

### 1. La caisse est tombée en panne 😪  

Le droit d'entrée journalier dans un parc d’attraction est de 37€ pour un adulte et de 28€ pour un enfant.   
Alice et Bob doivent faire payer les entrées, mais leur caisse est malheureusement tombée en panne, et la queue s’allonge très vite…   
On leur fourni en urgence une calculatrice qui dispose de Python pour les aider.  

* Un groupe de 2 adultes et 2 enfants se présente à la caisse. Il faut donc calculer :   $2 \times 37 + 2 \times 28$  
* Un groupe de 3 adultes et 5 enfants se présente à la caisse. Il faut donc calculer :    $3 \times 37 + 5 \times 28$    
* Un groupe de 1 adulte et 3 enfants se présente à la caisse. Il faut donc calculer :    $1 \times 37 + 3 \times 28$

 😢 Ces calculs sont très répétitifs, et prennent du temps. La queue continue à s’allonger …  

Ils remarquent qu’il suffirait de saisir le nombre d’adultes et le nombre d’enfants pour automatiser le calcul. Ils décident d’écrire une fonction en Python :  

???+ question "Exécuter ce script"

	Que se passe-t-il ?

    {{IDE('scripts/prix_1')}}

	??? success "Solution"

		😢 Il ne se passe rien ...

Si on **« appelle »** `prix(3, 2)` :   
•	3 est automatiquement affecté à la variable  `nbre_adultes`  
•	2 est automatiquement affecté à la variable  `nbre_enfants`

???+ question "Tester des appels de fonction"

	Recopier dans la console (à la main !) ci-dessous

	```pycon title=''
	prix(3, 2)
	```
	Exécuter, puis recopier dans la console (à la main !) ci-dessous

	```pycon title=''
	prix(2, 3)
	```

	{{ terminal() }}

???+ question "Vos propres essais"

	A vous ...

	{{ terminal() }}

!!! abstract "Résumé"

    👉 Alice et Bob ont créé la **fonction** dont le **nom** est `prix`, qui a deux **paramètres** `nbre_adultes` et `nbre_enfants`.

### 2. La situation d’Alice et Bob s'améliore 😊

La situation d’Alice et Bob s’est nettement améliorée, mais ils veulent aller encore plus vite.   
Ils voudraient juste saisir les nombres d’adultes et d’enfants, par exemple 3 et 2 , et ne pas avoir à écrire `prix(3, 2)`   
Pour cela, ils écrivent le script suivant : 

???+ question "Exécuter ce script"

	Que se passe-t-il ?

    {{IDE('scripts/prix_2')}}

!!! info "Ce qu'il s'est passé"

    👉La fonction `prix` a été appelée. Le résultat **renvoyé** par la fonction a été affecté à la variable `a_payer`. Cette variable a ensuite été affichée.


### 3. Le tarif étudiant

On vient signaler à Alice et Bob qu’un nouveau tarif entre en vigueur instantanément : le tarif « étudiant » à 30€.   

???+ question "💻 A vous de jouer 1 "

	En vous inspirant de ce que vous avez déjà vu, compléter ce script qui tient compte de ce nouveau tarif.

	Le tester pour 1 adulte, 2 étudiants, 3 enfants. Le prix à payer doit être 181 €.

    {{IDE('scripts/prix_3')}}

### 4. 🎂 Jour d'anniversaire   

Nouveauté : si c’est le jour d’anniversaire d’une personne du groupe, tout le groupe bénéficie d’une réduction de 10 %.

???+ question "💻 A vous de jouer 2 "

	Recopier **sans la modifier la fonction du 3.**, compléter le programme pour qu’il demande si c’est un jour anniversaire, et qu’il affiche le prix à payer suivant les cas.  

	On rappelle que pour diminuer un prix de 10%, il suffit de le multiplier par 0.9.  

    {{IDE()}}

	??? success "Solution"

		```python
		def prix(nbre_adultes, nbre_enfants, nbre_etudiants):
			resultat = 37  *nbre_adultes + 28 * nbre_enfants + 30 * nbre_etudiants
			return resultat

		anniv = input("Jour d'anniversaire : saisir en toutes lettres oui ou non ")
		adultes = int(input("nombre d'adultes ? "))
		enfants = int(input("nombre d'enfants ? "))
		etudiants = int(input("nombre d'étudiants ? "))
		a_payer = prix(adultes, enfants, etudiants)
		if anniv == "oui":
			a_payer = 0.9 * a_payer   
		print("A payer : ", a_payer)
		```

## II. Deuxième situation : les tables de multiplication

???+ question "Aider le petit frère de Bob"

	Le petit frère de Bob doit apprendre ses tables de multiplication.  
	
	Pensant l’aider, Bob écrit la fonction suivante :  

	Exécuter ce script. Que s'affiche-t-il ?

    {{IDE('scripts/tables')}}

	??? success "Solution"

		Il ne s'affiche que `9`

!!! info "😨 Ce qu'il s'est passé"

    👉 A noter : losque l'on rencontre l'instruction `return`, on sort de la fonction. Si un `return` se trouve dans une boucle `for`, on sort donc de la fonction dès la première rencontre de l'instruction `return`. 

???+ note dépliée

    Nous verrons dans le paragraphe IV. comment on peut faire.
	
	Mais d'abrd, regardons si une fonction peut renvoyer plusieurs choses.

## III. Et si l'on veut que notre fonction fasse plusieurs choses ? 

???+ question "Une fonction bizarre"

	Vous rencontrerez souvent des fonctions comme celle se trouvant dans le script ci-dessous :
	
	Exécuter ce script. Que s'affiche-t-il ?

    {{IDE('scripts/somme_produit')}}

	??? success "Solution"

		```pycon title=''
		(7, 10)
		<class 'tuple'>
		```



!!! info "😨 Ce qu'il s'est passé"

    👉Vous observez que des parenthèses sont apparues dans l’affichage du résultat. 

    En fait il y a **une seule variable** renvoyée, et non deux, qui est de type `tuple` (ici un "couple" de deux entiers), comme l’affichage de la ligne suivante le confirme. Nous étudierons plus tard les tuples.


## IV. Comment créer une fonction qui nous donne une table de multiplication ? 

👉 Méthode 1 :  
  
On écrit une fonction qui renvoie une « liste » ou un « tuple. »  
Une telle fonction renverra par exemple pour la table de 9   

* Si la fonction renvoie une liste : [9, 18, 27, 36, 45, 54, 63, 72, 81, 90]  
* Si la fonction renvoie un tuple : (9, 18, 27, 36, 45, 54, 63, 72, 81, 90)

Nous étudierons les listes et les tuples un peu plus tard

👉  Méthode 2 : avec une procédure : 

On peut écrire une fonction qui ne renvoie rien, mais qui fait des actions, comme par exemple des affichages. 

!!! info "procédure"

	👉 Une fonction qui ne renvoie rien s’appelle une procédure.

	De plus en plus on appelle ce type de fonction des ... fonctions.

???+ question "Table de multiplication"

	Exécuter le script ci-dessous et observer l'affichage obtenu.

    {{IDE('scripts/tables_2')}}

!!! info "Pas de `return ?`"

	👉 Dans une procédure, la ligne `return None` est facultative 

	Supprimez-là dans l'exercice ci-dessus, puis testez à nouveau.

## V. Une fonction sans paramètre ? 

???+ question "Jouons aux dés"

	Exécuter le script ci-dessous et observer l'affichage obtenu.

    {{IDE('scripts/lancer')}}


## VI. A retenir

!!! abstract "Résumé"   

    * Une fonction commence par le mot `def`, suivi du nom de la fonction, de la liste des paramètres entre parenthèses, et de :.
    Toutes les instructions d’une fonction sont indentées.

    * S’il n’y a pas de paramètres, il faut obligatoirement mettre des parenthèses vides.

    * Une fonction ne peut renvoyer qu’une seule variable. Cela se fait avec l’instruction `return`.

    * L’exécution de l’instruction `return` provoque obligatoirement la sortie de la fonction. Si d’autres instructions se trouvent après `return` elles ne seront jamais exécutées.

    * La valeur renvoyée est indiquée par l’instruction (optionnelle) `return`. Si celle-ci n’est pas présente, la valeur par défaut `None` est renvoyée.


## VII. 🤔 Appeler une fonction dans une fonction ... 

???+ question "Les monnaies"

	La fonction `euro_vers_yan` appelle deux autres fonctions. Cela ne pose aucun problème.

	Exécuter le script ci-dessous et observer l'affichage obtenu.

    {{IDE('scripts/euros_yuan')}}


!!! info "Remarque sur la "**portée des variables**""

	- `euros` est une variable locale dans la fonction `euros_vers_dollars`      
	- `dollars` est une variable locale dans la fonction `dollar_vers_yuan`  
	- `montant` une variable locale dans la fonction `euro_vers_yuan`.   
	
	Ces variables ne sont "connues" que dans ces fonctions.

### Conséquence 1 : choix du nom des paramètres

Nous aurions donc pu choisir pour chaque fonction le même nom de paramètre.


???+ question "Les monnaies"

	Exécuter, et tester le script suivant :

    {{IDE('scripts/euros_yuan_2')}}

!!! info "Variable locale `montant`"

	Ici nous avons trois variables locales différentes `montant` dans les trois fonctions du script.

### Conséquence 2 : une variable “locale”, n’est pas reconnue à "l’extérieur"

???+ question "Les monnaies"

	Exécuter, et tester le script suivant :

    {{IDE('scripts/euro_dollar')}}

!!! info "Variable locale `montant`"

	👉 La variable `resultat_dollars` n'est pas connue à l'extérieur de la fonction `euro_vers_dollar(montant):`


### Conséquence 3 : méthode souvent utilisée 

!!! abstract "🐘 A retenir"

    💡 On affecte à une variable la valeur renvoyée par une fonction

???+ question "Les monnaies"

	Exécuter, et tester le script suivant :

    {{IDE('scripts/euros_dollars')}}

!!! danger "Attention"

    ⚠️ Ne pas utiliser des noms de variables et des noms de fonctions identiques.  


## VIII. **Docstring** d’une fonction 

!!! info "Docstring"

	Nous reviendrons plus tard dans l’année sur les "docstring" d’une fonction, et ajouterons d’autres éléments.  Pour le moment, nous les utiliserons pour  expliquer le rôle d’une fonction.  

	Cette "docstring" est composée de lignes de texte écrites entre `"""` et `"""`.  

	Ces lignes ne sont pas exécutées par le programme, et simplement destinées à donner des explications à celui qui lit le code (script).  

	Par exemple : 
	```python title=''
	def euro_vers_dollar(montant):
		"""
		Cette fonction renvoie la valeur de montant euros convertie en dollars.
		par exemple euro_vers_dollar(2) doit renvoyer 2,38
		"""
		return montant * 1.19 # en supposant qu'un euro vaut 1,19 dollars
    ```

## IX. Exercice

Pour déterminer la distance d’arrêt d’une voiture on applique la formule suivante :

Distance d’arrêt (en m) = distance parcourue pendant le temps de réaction (en m) + distance de freinage (en m)

Une documentation donne les informations suivantes pour une route sèche :
Notations :

* V la vitesse d’un véhicule en km/h
* R la distance parcourue pendant le temps de réaction (en m)
* F la distance de freinage (en m)
* A la distance d’arrêt du véhicule (en m)

On donne les formules suivantes :

* R = V/3.6
* F = V²/200

Nous rappelons que les noms de variables en informatique doivent être explicites (avoir un sens) et commencer par une lettre minuscule.

Compléter le script suivant, pour que : si l’on saisit la vitesse d’un véhicule, le programme affiche les trois renseignements suivants : la distance parcourue pendant le temps de réaction (en m), la distance de freinage (en m), et enfin la distance d’arrêt totale du véhicule (en m).
Dans les fonctions données ci-dessous, il faut remplacer le mot pass par les instructions python nécessaires.

{{IDE('scripts/freinage', MAX_SIZE=35)}}




