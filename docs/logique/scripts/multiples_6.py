def est_multiple_de_6_v1(nbre):
    est_multiple_de_3 = (nbre % 3 == 0)
    est_multiple_de_2 = (nbre % 2 == 0)
    if ((est_multiple_de_3 == True) and (est_multiple_de_2 == True)) == True:
        return True
    else:
        return False
        
def est_multiple_de_6_v2(nbre):
    est_multiple_de_3 = (nbre % 3 == 0)
    est_multiple_de_2 = (nbre % 2 == 0)
    return ...
    
print(est_multiple_de_6_v1(12))
print(est_multiple_de_6_v1(13))
print(est_multiple_de_6_v2(12))
print(est_multiple_de_6_v2(13))
