---
author: Mireille Coilhac
title: 🏡 Accueil
---


😊 Bienvenue sur la page de NSI des classes de 1ère du Lycée Saint-Aspais de MELUN

Cours de première NSI (classes de Mme Coilhac)

😊 Ce site évoluera pendant l'année ...

!!! info "Exercices Python : Utilisation de l'IDE sur le site"


    Pour la plupart des exercices il y a un IDE possédant 5 boutons.


    ### Tests publics

    Vous devez compléter le code dans la zone de saisie. Les assertions constituent les **tests publics**. Il reprennent le plus souvent les exemples de l'énoncé.

    Une ligne du type `#!py assert somme(10, 32) == 42` vérifie que la fonction `somme` renvoie bien `#!py 42` lorsqu'on lui propose `#!py 10` et `#!py 32` comme arguments.

    Vous pouvez vérifier que votre fonction passe ces tests publics en cliquant sur le bouton <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-play-64.png" alt="Exécuter les tests"></button><span>.

    ### Tests privés

    Une fois les tests publics passés, vous pouvez passer les **tests privés** en cliquant sur le bouton **Valider** <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-check-64.png" alt="Valider"></button><span>.

    Ceux-ci sont plus nombreux et, comme leur nom l'indique, ne vous sont pas connus. Seul leur résultat vous est indiqué avec, parfois, un commentaire sur la donnée ayant mis en défaut votre code.

    Dans la plupart des exercices, un compteur permet de suivre vos essais. Ce compteur est décrémenté à chaque fois que vous cliquez sur le bouton **Valider** <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-check-64.png" alt="Valider"></button><span> effectuant les tests privés. Lorsqu’il atteint 0, la solution de l’exercice vous est proposée.


    ### Autres boutons

    Il est aussi possible de :

    * <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-download-64.png" alt="Télécharger"></button><span> : télécharger le contenu de l'éditeur si vous souhaitez le conserver ou travailler en local ;

    * <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-upload-64.png" alt="Téléverser"></button><span> : téléverser un fichier Python dans l'éditeur afin de rapatrier votre travail local ;

    * <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-restart-64.png" alt="Réinitialiser l'IDE"></button><span> : recharger l'éditeur dans son état initial ;

    * <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-save-64.png" alt="Sauvegarder"></button><span> : sauvegarder le contenu de l'éditeur dans la mémoire de votre navigateur ;


## Crédit

Pour la présentation des IDE : CodEX

_mise à jour le 05/06/2024_
